import React from 'react';
import { shallow } from 'enzyme';

// component
import CardLoadingItem from './CardLoadingItem';

describe('CardLoadingItem', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<CardLoadingItem to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
