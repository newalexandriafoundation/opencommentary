import { EditorState } from 'draft-js';

const clearInlineStyles = (editorState) => {
	const currentStyle = editorState.getCurrentInlineStyle();

	return EditorState.setInlineStyleOverride(editorState, currentStyle.clear());
};

export default clearInlineStyles;
