/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query textGroupsQuery(
		$textSearch: String
		$urn: CtsUrn
		$limit: Int
		$offset: Int
	) {
		textGroups(
			textsearch: $textSearch
			urn: $urn
			limit: $limit
			offset: $offset
		) {
			id
			slug
			title
			urn
			works {
				language {
					id
					title
					slug
				}
			}
		}
	}
`;

export default query;
