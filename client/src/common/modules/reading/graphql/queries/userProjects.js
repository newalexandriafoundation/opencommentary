/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query userProjectsQuery(
		$limit: Int
		$offset: Int
	) {
		userProjects(
			limit: $limit
			offset: $offset
		) {
			_id
			activity {
				user {
					_id
					username
					avatar
				}
				type
				excerpt
				url
			}
			createdAt
			description
			email
			hostname
			slug
			status
			subtitle
			tags
			title
			twitter
			updatedAt
			url
		}
	}
`;

export default query;
