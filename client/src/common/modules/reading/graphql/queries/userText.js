/**
 * @prettier
 */

import gql from 'graphql-tag';

const query = gql`
	query userTextsQuery(
		$search: String
		$status: EnumUserTextStatus
		$commentaryID: String
		$contentType: EnumUserTextContentType
		$urn: String
	) {
		userTextMany(
			filter: {
				search: $search
				status: $status
				commentaryID: $commentaryID
				contentType: $contentType
				urn: $urn
			}
		) {
			_id
			commentaryID
			contentType
			createdAt
			latestRevision {
				_id
				created
				tenantId
				text
				title
			}
			revisions {
				_id
				created
				createdBy
				originalDate
				tenantId
				text
				title
			}
			updatedAt
			urn
			userID
		}
	}
`;

export default query;
