/**
 * @prettier
 */

import gql from 'graphql-tag';

const createUserTextMutation = gql`
	mutation userTextCreate($userText: UserTextInput!) {
		userTextCreate(userText: $userText) {
			_id
		}
	}
`;

export default createUserTextMutation;
