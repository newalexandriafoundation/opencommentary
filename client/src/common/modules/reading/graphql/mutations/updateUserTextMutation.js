/**
 * @prettier
 */

import gql from 'graphql-tag';

const updateUserTextMutation = gql`
	mutation userTextUpdate($record: UpdateByIdUserTextInput!) {
		userTextUpdateByID(record: $record) {
			recordId
		}
	}
`;

export default updateUserTextMutation;
