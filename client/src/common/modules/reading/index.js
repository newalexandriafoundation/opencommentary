import * as React from 'react';
import PropTypes from 'prop-types';
import { Route, Routes } from 'react-router-dom';

import App from './components/App';
import Root from '#common/containers/Root';

const Reading = ({ project }) => {
	return (
		<Root>
      <Routes>
  			<Route path="/:urn" element={<App project={project} />} />
      </Routes>
		</Root>
	);
};

Reading.propTypes = {
	project: PropTypes.object,
};

export default Reading;
