let simpleDOMBuilder = function(){};

if (process.env.REACT_APP_IS_ON_SERVER === 'SERVER') {
	// https://github.com/facebook/draft-js/issues/586#issuecomment-300347678
	// which was inspired by https://github.com/facebook/draft-js/blob/master/src/model/paste/getSafeBodyFromHTML.js
	simpleDOMBuilder = html => {
		// only load JSDOM and set these globals if we're on the server
		if (typeof document === 'undefined') {
			const jsdom = require('jsdom');
			const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
			const _window = dom.window;

			global.document = _window.document;
			global.HTMLElement = _window.HTMLElement;
			global.HTMLAnchorElement = _window.HTMLAnchorElement;
			global.Node = _window.Node;
		}

	  const doc = document.implementation.createHTMLDocument('mydoc');
	  doc.documentElement.innerHTML = html;
	  const root = doc.getElementsByTagName('body')[0];
	  return root;
	}
} else {
	simpleDOMBuilder = html => {
		const doc = document.implementation.createHTMLDocument('mydoc');
		doc.documentElement.innerHTML = html;
		const root = doc.getElementsByTagName('body')[0];
		return root;
	}
}

export default simpleDOMBuilder;