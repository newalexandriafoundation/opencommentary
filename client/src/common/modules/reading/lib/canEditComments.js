export default function canEditComments() {
	if (typeof window !== 'undefined') {
		return window.USER_IS_ADMIN || window.USER_IS_EDITOR || window.USER_IS_COMMENTER;
	} else if (typeof global !== 'undefined') {
		return global.USER_IS_ADMIN || global.USER_IS_EDITOR || global.USER_IS_COMMENTER;
	};

	return false;
}
