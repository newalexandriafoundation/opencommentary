/**
 * @prettier
 */

// https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
function fallbackCopyToClipboard(str) {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
          document.getSelection().rangeCount > 0
          ? document.getSelection().getRangeAt(0)
          : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    if (selected) {
        document.getSelection().removeAllRanges();
        document.getSelection().addRange(selected);
    }
}

export default function copyToClipboard(str) {
    if (!navigator.clipboard) {
        fallbackCopyToClipboard(str);
        return;
    }

    navigator.clipboard.writeText(str).then(function() {
        console.log('copied!');
    }, function(err) {
        console.error('failed to copy:', err)
    });
}
