/**
 * @prettier
 */

import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Cookies from 'universal-cookie';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Modal from '@material-ui/core/Modal';
import NativeSelect from '@material-ui/core/NativeSelect';
import { gql, useMutation } from '@apollo/client';
import { makeStyles } from '@material-ui/core/styles';

import OrpheusEditor from '#common/modules/orpheus-editor/components/Editor';
import createBaseEditorState from '#common/modules/reading/lib/createBaseEditorState';
import decorators from '#common/modules/orpheus-editor/components/decorators';
import makeRequest from '#common/modules/reading/lib/makeRequest';

import APP_INIT_QUERY from '#common/modules/reading/components/App.graphql';
import { ReadingEnvDispatch, SET_REVISIONS_MODAL_STATE } from '../../constants';

const UPDATE_USER_TEXT = gql`
	mutation updateUserText($id: Int!, $userText: UserText) {
	  updateUserTextResponse(id: $id, input: $userText) @rest(type: "UserText", path: "usertexts/{args.id}/", method: "PUT") {
	    id
	    latest_revision_text
	  }
	}
`;

const useStyles = makeStyles((theme) => ({
	formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
	paper: {
		backgroundColor: theme.palette.background.paper,
		borderRadius: 2,
		boxShadow: theme.shadows[5],
		left: '50%',
		outline: 0,
		padding: theme.spacing(2, 4, 3),
		position: 'absolute',
		top: '50%',
		transform: 'translate(-50%, -50%)',
		width: 600,
	},
}));

RevisionsModal.propTypes = {
	open: PropTypes.bool,
	refetch: PropTypes.func,
	revisable: PropTypes.shape({ revisions: PropTypes.array }),
};

RevisionsModal.defaultProps = {
	refetch: () => {},
	revisable: { revisions: [] },
};

function RevisionsModal({
	open = false,
	refetch = () => {},
	revisable = {}
}) {
	const classes = useStyles();
	const dispatch = useContext(ReadingEnvDispatch);
	const [updateUserText] = useMutation(UPDATE_USER_TEXT);
	const [selectedRevision, setSelectedRevision] = useState(revisable && revisable.revisions[0]);
	const [selectedRevisionId, setSelectedRevisionId] = useState(revisable && revisable.revisions[0] && revisable.revisions[0].id);

	useEffect(() => {
		const revisionId = (revisable || {}).selected_revision_id;
		const revisions = (revisable || {}).revisions;

		setSelectedRevisionId(revisionId);

		if (Array.isArray(revisions)) {
			setSelectedRevision(revisions.find(r => r.id === revisionId));
		}
	}, [JSON.stringify(revisable), open]);

	const handleModalClose = () => {
		dispatch({
			type: SET_REVISIONS_MODAL_STATE,
			modalOpen: false,
		});
	};

	const handleChange = e => {
		const revision = revisable.revisions.find(r => r.id.toString() === e.target.value);

		setSelectedRevisionId(e.target.value);
		setSelectedRevision(revision);
	};

	const updateRevisable = async e => {
		e.preventDefault();
		e.stopPropagation();

		if (!selectedRevision) {
			console.warn('No revision selected!');
			return;
		}

		try {
			const selectedId = parseInt(selectedRevisionId, 10);
			const response = await updateUserText({
				variables: {
					id: revisable.id,
					userText: {
						...revisable,
						latest_revision_text: selectedRevision.text,
						selected_revision_id: selectedId,
					}
				}
			});

			refetch();

			handleModalClose();

			// TODO: (charles) wire up optimistic response to the reading env
		} catch (e) {
			console.error('Something went wrong updating that userText:', e);
		}
	}

	return (
		<Modal
			aria-labelledby="Text node history modal"
			aria-describedby="Provides an interface to a text nodes' edition history."
			onClose={handleModalClose}
			open={open}
		>
			<div className={classes.paper}>
				{selectedRevision && (
					<OrpheusEditor
						editorState={createBaseEditorState(selectedRevision, decorators)}
						readOnly
					/>
				)}
				<FormControl className={classes.formControl}>
					<InputLabel htmlFor="revision">Revision</InputLabel>
					<NativeSelect
						value={selectedRevisionId || ''}
						onChange={handleChange}
					>
						<option value=""></option>
						{(revisable && revisable.revisions || []).map(r => {
							// const contentState = convertFromRaw(JSON.parse(r.text));
							// const text = contentState && `${contentState.getPlainText().substring(0, 20)}` || 'Could not get plain text'

							return (
								<option key={r.id} value={r.id}>{r.created_at}</option>
							);
						})}
					</NativeSelect>
				</FormControl>
				<div>
					<Button
						variant="contained"
						color="primary"
						onClick={updateRevisable}
					>
						Set
					</Button>
					<Button onClick={handleModalClose}>Cancel</Button>
				</div>
			</div>
		</Modal>
	);
}

export default RevisionsModal;
