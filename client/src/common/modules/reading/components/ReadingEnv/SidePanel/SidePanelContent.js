/**
 * @flow
 * @prettier
 */

import * as React from 'react';
import { useCallback, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Box from '@material-ui/core/Box';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useQuery } from '@apollo/client';

import SidePanelAbout from './SidePanelAbout';
import SidePanelCommentary from './SidePanelCommentary';
import SidePanelText from './SidePanelText';
import { ReadingEnvDispatch, SET_SIDE_PANEL_STATE } from '#common/modules/reading/constants';

import getUrnFromUrl from '#common/modules/reading/lib/getUrnFromUrl';

import editionsAndTranslationsQuery from '../../../graphql/queries/editionsAndTranslations';

const EditionsPanel = React.lazy(() => import('#common/modules/reading/components/ReadingEnv/EditionsPanel'));
const TranslationsPanel = React.lazy(() => import('#common/modules/reading/components/ReadingEnv/TranslationsPanel'));

const useStyles = makeStyles(theme => ({
	heading: {
	  fontSize: theme.typography.pxToRem(15),
	  fontWeight: theme.typography.fontWeightRegular,
	},
}));

const Accordion = withStyles({
	expanded: {
		overflowY: 'scroll',
	}
})(MuiAccordion);

const AccordionDetails = withStyles({
	root: {
		maxHeight: 400,
		overflowY: 'scroll',
	}
})(MuiAccordionDetails);

function getTabPanelFor(
	label: string,
	urn: string,
	focusedUrn?: string
): React.Node {
	let contents = null;

	if (label === 'about') {
		contents = <SidePanelAbout urn={urn} />;
	} else if (label === 'editions') {
		contents = <EditionsPanel urn={focusedUrn || urn} />
	} else if (label === 'translations') {
		contents = <TranslationsPanel urn={urn} />;
	} else if (label === 'commentary') {
		return null;
	}

	return (
		<Box p={3}>
		  {contents}
		</Box>
	);
}

type SidePanelContentProps = {
	focusedUrn: string,
	openCommentForm: () => void,
	refetchComments: () => void,
	text: Object,
};

const SidePanelContent = ({
	focusedUrn,
	openCommentForm,
	refetchComments,
	text,
}: SidePanelContentProps) => {
	const dispatch = useContext(ReadingEnvDispatch);

	const [commentaryExpanded, setCommentaryExpanded] = useState(false);
	const { work } = text;
	const { full_urn: fullUrn, urn: baseUrn } = work;
	const { loading, error, data } = useQuery(editionsAndTranslationsQuery, {
		variables: { urn: baseUrn },
	});
	const classes = useStyles();

	useEffect(() => {
		if (focusedUrn) {
			setCommentaryExpanded(true);
		}
	}, [focusedUrn]);

	const handleExpandCommentary = useCallback(_e => {
		const nextState = !commentaryExpanded;

		if (!nextState) {
			dispatch({
				type: SET_SIDE_PANEL_STATE,
				// reset currentComment when the accordion closes,
				// so that users can go back to viewing all
				// of the comments instead of just a selected
				// one
				currentComment: {},
				// keep the SidePanel open
				sidePanelOpen: true,
			});
		}
		setCommentaryExpanded(nextState);
	});

	if (loading) {
		return <LinearProgress />;
	}

	if (error) {
		return <p className="error">There was an error. Check the console for more information.</p>;
	}

	const { works: works } = data;
	const { editions, translations, versions } = works.reduce(
		(ws, w) => {
			if (w.exemplar) {
				ws.editions.push(w);
			} else if (w.translation) {
				ws.translations.push(w);
			} else if (w.version) {
				ws.versions.push(w);
			}

			return ws;
		},
		{ editions: [], translations: [], versions: [] }
	);

	const tabs = [
		{ label: 'About' },
		{ label: 'Editions' },
		{ label: 'Translations' },
	];

	return (
		<div className="sidePanelContentContainer">
			<div className="hide-scrollbars">
				{tabs.map((t, i) => {
					return (
						<Accordion elevation={0} key={`panel-header-${t.label}-${i}`}>
							<AccordionSummary
								aria-controls={`panel-${t.label}-content`}
								expandIcon={<ExpandMoreIcon />}
								id={`panel-{t.label}-header`}
							>
								<Typography className={classes.heading}>{t.label}</Typography>
							</AccordionSummary>
							<AccordionDetails>
								<React.Suspense fallback={<LinearProgress />}>
									{getTabPanelFor(t.label.toLowerCase(), baseUrn, focusedUrn)}
								</React.Suspense>
							</AccordionDetails>
						</Accordion>
					);
				})}
				<Accordion elevation={0} expanded={commentaryExpanded} onChange={handleExpandCommentary}>
					<AccordionSummary
						aria-controls="panel-commentary-content"
						expandIcon={<ExpandMoreIcon />}
						id="panel-commentary-header"
					>
						<Typography className={classes.heading}>Commentary</Typography>
					</AccordionSummary>
					<AccordionDetails>
						<React.Suspense fallback={<LinearProgress />}>

								<SidePanelCommentary
									openCommentForm={openCommentForm}
									urn={focusedUrn || getUrnFromUrl() || fullUrn}
									work={work}
								/>

						</React.Suspense>
					</AccordionDetails>
				</Accordion>
			</div>
		</div>
	);
};

SidePanelContent.propTypes = {
	focusedUrn: PropTypes.string,
	openCommentForm: PropTypes.func,
	refetchComments: PropTypes.func,
	text: PropTypes.shape({ work: PropTypes.shape({ urn: PropTypes.string }) }),
};

export default SidePanelContent;
