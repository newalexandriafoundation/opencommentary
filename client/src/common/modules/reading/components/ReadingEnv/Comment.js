/**
 * @prettier
 */

import React, { useCallback, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import {
	ContentState,
	convertFromHTML,
	convertFromRaw,
	EditorState,
} from 'draft-js';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';
import ShareIcon from '@material-ui/icons/Share';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import Popover from '@material-ui/core/Popover';
import format from 'date-fns/format';
import { kebabCase } from 'lodash';

import CommentTags from './CommentTags';
import OrpheusEditor from '../../../orpheus-editor/components/Editor';
import decorators from '../../../orpheus-editor/components/decorators';

import canEditComments from '../../lib/canEditComments';
import copyToClipboard from '../../lib/copyToClipboard';
import makeRequest from '../../lib/makeRequest';
import markCitations from '../../lib/markCitations';

import { ReadingEnvDispatch, SET_CURRENT_COMMENT } from '../../constants';

const formatCommenterName = commenter => {
	return `${commenter.first_name} ${commenter.last_name}`;
}

const formatCommenterNameForBibliography = commenter => {
	return `${commenter.last_name}, ${commenter.first_name}`;
}

const getPort = () => {
	if (window.location.port && window.location.port !== '') {
		return `:${window.location.port}`;
	}

	return '';
};

const formatBibliography = comment => {
	const author = formatCommenterNameForBibliography(comment.commenters[0]);
	const website = 'The Open Commentary Platform';
	const organization = 'The Open Commentary Platform Foundation';
	const _latestRevision = comment.revisions[0];
	const title = _latestRevision.title || 'The Open Commentary Platform Comments';
	const date = format(new Date(_latestRevision.updated_at), 'd MMMM y');
	const accessDate = format(new Date(), 'd MMMM y');
	const url = `https://${window.location.hostname}${getPort()}/comments/${comment.id}/${kebabCase(title)}`;

	return `${author}. "${title}." ${website}. ${organization}, ${date}. Accessed ${accessDate}. ${url}.`;
};

const deleteComment = async id =>
	makeRequest(`comments/${id}/`, {
		method: 'DELETE',
	});

Comment.propTypes = {
	comment: PropTypes.shape({
		id: PropTypes.number.isRequired,
		revisions: PropTypes.array.isRequired,
		urn: PropTypes.string.isRequired,
	}),
	highlight: PropTypes.bool,
	removeComment: PropTypes.func,
};

function Comment({
	comment,
	removeComment,
}) {
	const dispatch = useContext(ReadingEnvDispatch);
	const [tags, setTags] = useState(comment.tags.slice());
	const [shareText, setShareText] = useState('Copy citation to clipboard');

	const _editComment = useCallback(
		e => {
			dispatch({
				type: SET_CURRENT_COMMENT,
				currentComment: comment,
			});
		},
		[comment.id]
	);

	const _deleteComment = useCallback(
		async e => {
			e.preventDefault();
			e.stopPropagation();

			if (window.confirm('Are you sure you want to delete this comment?')) {
				const id = comment.id;
				const resp = await deleteComment(id);

				if (resp) {
					removeComment(id);
				}
			}
		},
		[comment.id]
	);

	const _shareComment = useCallback(
		e => {
			const bib = formatBibliography(comment);

			copyToClipboard(bib);

			setShareText('Copied!');

			setTimeout(() => {
				setShareText('Copy citation to clipboard');
			}, 3000);
		},
		[comment.id]
	);

	const _openRevisionsMenu = useCallback(
		e => {
			// todo comment revisions;
			console.log(comment);
		},
		[comment.id]
	);

	// const revision = comment.revisions[0];
	const [revision, setRevision] = useState(comment.revisions[0]);
	if (!revision) {
		return null;
	}

	const urn = comment.urn;

	// mark citations in text
	// can be added to orpheus editor with predictive citation model in the future
	// FIXME: markCitations()
	// needs to be done in a decorator, otherwise it breaks the JSON parsing
	// of the raw text and causes the JSON to be rendered as a string.
	// const revisionText = markCitations(revision.text);

	let baseContent;
	try {
		baseContent = convertFromRaw(JSON.parse(revision.text));
	} catch (e) {
		// if we've failed to parse the JSON, we can assume that markCitations should be safe
		const { contentBlocks, entityMap } = convertFromHTML(
			markCitations(revision.text)
		);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	const [anchorEl, setAnchorEl] = React.useState(null);

	const handleOpenPopover = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClosePopover = () => {
		setAnchorEl(null);
	};
	const popoverOpen = Boolean(anchorEl);

	return (
		<div
			className="comment"
			key={comment.id}
			data-commentid={comment.id}
			data-urn={urn}
		>
			<div className="commentHead">
				<div className="commentHeadText">
					{revision.title ? <h2>{revision.title}</h2> : ''}
				</div>
				<div className="commentHeadMeta">
					<span className="commentByline">
						{comment.commenters && comment.commenters.length ?
							<a href={`/commenters/${comment.commenters[0].username}`} className="commenterLink">
								{comment.commenters[0].picture ?
									<div
										className="chipAvatar"
										style={{
											backgroundImage:
												`url(${comment.commenters[0].picture})`,
										}}
									/>
								: ''}
								<span>{formatCommenterName(comment.commenters[0])}</span>
							</a>
						: ''}
						{format(new Date(revision.created_at), 'd MMM yy')}
					</span>
					<div className="commentActions flex">
						<span
							title={shareText}
							role="button"
							className="dark-gray hover-opaque ml2 mr2 muted pointer"
							onClick={_shareComment}
						>
							<ShareIcon />
						</span>
						{canEditComments() ? (
							<>
								<span
									role="button"
									className="dark-gray hover-opaque muted pointer"
									onClick={_editComment}
								>
									<EditIcon />
								</span>
								<span
									role="button"
									className="dark-gray hover-opaque ml1 muted pointer"
									onClick={_deleteComment}
								>
									<DeleteOutlineIcon />
								</span>
							</>
						) : null}
						<span
							role="button"
							className="dark-gray hover-opaque ml1 muted pointer"
							onClick={handleOpenPopover}
						>
							<MoreHorizIcon />
						</span>
						<Popover
							id="revision_popover"
							open={popoverOpen}
							anchorEl={anchorEl}
							onClose={handleClosePopover}
							anchorOrigin={{
								vertical: 'bottom',
								horizontal: 'center',
							}}
							transformOrigin={{
								vertical: 'top',
								horizontal: 'center',
							}}
						>
							{comment.revisions.map((revision, i) => (
								<button
									key={revision.created_at}
									className="revisionSelectButton p1 "
									onClick={setRevision.bind(this, revision)}
								>
									{format(new Date(revision.created_at), 'd MMM yy')}
								</button>
							))}
						</Popover>
					</div>
				</div>
			</div>
			<div className="flex">
				<div className="commentBody">
					<OrpheusEditor
						editorState={EditorState.createWithContent(baseContent, decorators)}
						readOnly
					/>
				</div>
			</div>
			<CommentTags
				commentId={comment.id}
				tags={tags || []}
			/>
		</div>
	);
}

export default Comment;
