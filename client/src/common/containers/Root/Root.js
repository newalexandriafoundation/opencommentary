import React from 'react';
import PropTypes from 'prop-types';
import { Provider as ReduxProvider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import { SnackbarProvider } from 'notistack';

import store from '#common/store';

const Root = ({ children }) => (
	<ReduxProvider store={store}>
		<CookiesProvider>
			<SnackbarProvider anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
  		}}>
				{children}
			</SnackbarProvider>
		</CookiesProvider>
	</ReduxProvider>
);

Root.propTypes = {
	children: PropTypes.node,
};

export default Root;
