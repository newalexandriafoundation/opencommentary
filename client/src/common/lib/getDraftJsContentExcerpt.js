import _s from 'underscore.string';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';

/**
 * getDraftJsContentExcerpt
 * @param {string} bodyContent - json from draftjs state stored as string in db
 * @param {number} pruneLength - how short to make the excerpt
 * @returns {string} excerpt of body content pruned at specified limit
 */
const getDraftJsContentExcerpt = ({
	bodyContent = JSON.stringify(convertToRaw(EditorState.createEmpty().getCurrentContent())),
	pruneLength = 170
}) => {
	// parse body content string to json
	const body = JSON.parse(bodyContent);

	// get content state from raw json stored in db
	let contentState;
	try {
		contentState = convertFromRaw(body);
	} catch (e) {
		// For now, don't log the errors and return an empty string
		// console.error(e);
		return '';
	}

	// get block map from content state and extract text from each block
	const blockMap = contentState.getBlockMap();
	const blocksText = [];
	blockMap.mapEntries((entry) => {
		blocksText.push(entry[1].getText());
	});

	return _s.prune(blocksText.join(' '), pruneLength);
};

export default getDraftJsContentExcerpt;
