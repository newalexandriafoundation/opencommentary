// This file is necessary to ensure that redirects based on an archive's hostname
// go to the appropriate stage.  There is a convention that rewrites the archive's hostname to
// include .orphe.us instead of saving it with it's stage hostname, e.g., orpheus.local:3000.
// See ArchiveEditorContainer.js, handleSubmit() for context.

const getCurrentArchiveBaseUrl = (hostname = '') => {
	if (window.location.hostname.includes('orpheus.local')) {
		return window.location.protocol + '//' + hostname.split('.')[0] + '.orpheus.local:3000';
	} else if (window.location.hostname.includes('staging')) {
		return window.location.protocol + '//' + hostname.split('.')[0] + '.staging.orphe.us';
	} else  {
		return window.location.protocol + '//' + hostname;
	}
};

export default getCurrentArchiveBaseUrl;
