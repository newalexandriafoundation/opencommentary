/**
 * @flow
 * @prettier
 */

import * as React from 'react';

import createEditorState from './createEditorState';

import OrpheusEditor from '#common/modules/orpheus-editor/components/Editor';
import deriveLocationFromUrn from '#common/modules/reading/lib/deriveLocationFromUrn';

type NodeType = {
	location: $ReadOnlyArray<number>,
	revisions: $ReadOnlyArray<{ text: string }>,
	urn: string,
};

export default function renderUserTextNodes(nodes: Array<NodeType>): Array<React.Node> {
	return nodes
		.map((n, i) => {
			const location = n.location || deriveLocationFromUrn(n.urn);
			const urn = n.urn;
			const latestRevision = n.revisions[0] || {};
			const { text } = latestRevision;

			if (!text) return null;

			const editorState = createEditorState(text);

			if (!editorState) return null;

			return (
				<div
					className="flex hide-child"
					key={`${n.urn}-${i}`}
					style={{ fontSize: '1.25rem', lineHeight: '1.6em' }}
				>
					<div
						className="child ease-in-out fw7 gray-dark left pointer unselectable"
						style={{ fontSize: 16, maxWidth: 70, width: 70 }}
					>
						{location.join('.')}
					</div>
					<div
						data-urn={urn}
						data-location={JSON.stringify(location)}
						style={{ flexGrow: 1 }}
					>
						<OrpheusEditor editorState={editorState} readOnly />
					</div>
				</div>
			);
		})
		.filter(n => n !== null);
};
