/**
 * @flow
 * @prettier
 */

import * as React from 'react';

import createEditorState from './createEditorState';

import OrpheusEditor from '#common/modules/orpheus-editor/components/Editor';
import deriveLocationFromUrn from '#common/modules/reading/lib/deriveLocationFromUrn';

type CanonicalNodeType = {
	index: number,
	location: $ReadOnlyArray<number>,
	text: string,
	urn: string,
};

export default function renderCanonicalTextNodes(nodes: Array<CanonicalNodeType>): Array<React.Node> {
	return nodes
		.map((n, i) => {
			const location = n.location || deriveLocationFromUrn(n.urn);
			const { text, urn } = n;

			if (!text) return null;

			const editorState = createEditorState(text);

			if (!editorState) return null;

			return (
				<div
					className="flex "
					key={`${n.urn}-${i}`}
					style={{ fontSize: '1.25rem', lineHeight: '1.6em' }}
				>
					<div
						className="child ease-in-out fw7 gray-dark left pointer unselectable textNodeLocation"
					>
						{location.join('.')}
					</div>
					<div
						data-urn={urn}
						data-location={JSON.stringify(location)}
						className="textNode"
					>
						<OrpheusEditor editorState={editorState} readOnly />
					</div>
				</div>
			);
		})
		.filter(n => n !== null);
};
