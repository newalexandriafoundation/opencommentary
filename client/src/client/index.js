import * as React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/client'
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';

import CommentariesView from './modules/commentaries/components/CommentariesView';
import Reading from '#common/modules/reading';
import TextsView from './modules/texts/components/TextsView';
import apolloClient from '#client/middleware/apolloClient';
import customTheme from '#common/lib/muiTheme';

function Main() {
	React.useEffect(() => {
		const serverSideStyles = document.getElementById('mui-server-side-jss');

		if (serverSideStyles) {
			serverSideStyles.parentElement.removeChild(serverSideStyles);
		}
	}, []);

	return (
		<ThemeProvider theme={customTheme}>
			<ApolloProvider client={apolloClient}>
				<BrowserRouter basename="/read">
					<Reading
						project={JSON.parse(document.getElementById('project').textContent)} />
				</BrowserRouter>
			</ApolloProvider>
		</ThemeProvider>
	);
}

if (window.SSR_VIEW) {
	ReactDOM.hydrate(
		<Main />,
		document.getElementById('root')
	);
} else {
	// Use components on django frontend
	// see https://github.com/dhmit/rereading/blob/master/frontend/src/index.js
	window.app_modules = {
		React, // Make React accessible from the base template
		ReactDOM, // Make ReactDOM accessible from the base template

		// Add all frontend views here
		Main,
		CommentariesView,
		TextsView,
	};
}
