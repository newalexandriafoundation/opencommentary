import React from 'react';
import { shallow } from 'enzyme';

// component
import Modal from './Modal';

describe('Modal', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<Modal closeModal={() => {}}>
				<span>
					Modal content
				</span>
			</Modal>
		);
		expect(wrapper).toBeDefined();
	});
});
