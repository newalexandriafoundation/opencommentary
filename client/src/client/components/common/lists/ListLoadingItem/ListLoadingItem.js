import React from 'react';

import CardLoadingItem from '#common/components/common/cards/CardLoadingItem';


const ListLoadingItem = () => (
	<div className="list">
		<div className="listItem">
			<CardLoadingItem />
		</div>
	</div>
);

export default ListLoadingItem;
