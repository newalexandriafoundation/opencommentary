/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import _s from 'underscore.string';
import stripTags from 'underscore.string/stripTags';

import ListLoadingItem from '../ListLoadingItem';
import ListLoadingComment from '../ListLoadingComment';
import Card from '#common/components/common/cards/Card';
// import { toggleItemSelected } from '../../../../modules/items/actions';
import formatItemDataForCard from '#common/lib/formatItemDataForCard';

import { Checkbox } from '@material-ui/core';

const convertDescriptionToPlainText = jsonString => {
	if (!jsonString) return;
	let str = '';
	try {
		const json = JSON.parse(jsonString);
		str = json.blocks.map(b => b.text).join(' ');
	} catch (e) {
		// console.error(`Failed to parse item description: ${e}`);
		str = jsonString;
	}

	return stripTags(str);
};

const renderItem = (item, isPosts, isAdmin) => {
	let card;
	const {
		itemUrl,
		metadata,
		imageUrl,
		videoSrc,
		audioSrc,
		pdfSrc,
		mediaUrl,
	} = formatItemDataForCard(item);
	let cardItemUrl = isAdmin ? `${itemUrl}/edit` : itemUrl;

	if (item.type === 'text') {
		let item_description = '';
		if (item.description) {
			item_description = _s.prune(
				convertDescriptionToPlainText(item.description),
				240
			);
		}
		let counts = [
			{ label: 'Language', value: item.language },
			{ label: 'Editions', value: item.editions_count },
			{ label: 'Translations', value: item.translations_count },
		];
		if (!item || !item.default_text || !item.default_text.full_urn) {
			return null;
		}

		card = (
			<Card
				href={`/read/${item.default_text.full_urn}`}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.english_title}
				textLong={item_description}
				metadata={metadata}
				type="text"
				horizontal={true}
				textShort={item.textGroup}
				counts={counts}
				assetImageSrc={item.image_url}
			/>
		);
	} else if (item.type === 'commentary') {
		let counts = [
			{ label: 'Comments', value: item.comments_count },
		];
		card = (
			<Card
				href={`//${item.slug}.oc.newalexandria.info/`}
				key={_s.slugify(item.title)}
				className="listItemCard"
				textShort={item.textShort && 'By ' + item.textShort}
				title={item.title}
				textLong={_s.prune(
					convertDescriptionToPlainText(item.description),
					240
				)}
				metadata={metadata}
				assetImageSrc={imageUrl}
				assetImageWidth={1000}
				assetImageAlt={item.title}
				horizontal={true}
				type="commentary"
				counts={counts}
			/>
		);
	} else if (imageUrl) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textShort={_s.prune(
					convertDescriptionToPlainText(item.description),
					150
				)}
				assetImageSrc={imageUrl}
				assetImageWidth={1000}
				assetImageAlt={item.title} // is that correct?
				metadata={metadata}
			/>
		);
	} else if (videoSrc) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textShort={_s.prune(
					convertDescriptionToPlainText(item.description),
					150
				)}
				assetVideoSrc={videoSrc}
				metadata={metadata}
			/>
		);
	} else if (audioSrc) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textShort={_s.prune(
					convertDescriptionToPlainText(item.description),
					150
				)}
				assetAudioSrc={audioSrc}
				metadata={metadata}
			/>
		);
	} else if (pdfSrc) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textShort={_s.prune(
					convertDescriptionToPlainText(item.description),
					150
				)}
				assetPdfSrc={pdfSrc}
				metadata={metadata}
			/>
		);
	} else if (mediaUrl) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textShort={_s.prune(
					convertDescriptionToPlainText(item.description),
					150
				)}
				assetMediaSrc={mediaUrl}
				metadata={metadata}
			/>
		);
	} else if (item.commenter) {
		card = (
			<Card
				to={cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textLong={_s.prune(item.body, 200)}
				metadata={metadata}
				footer={{
					authorURL: `/@${item.commenter.slug}`,
					authorName: item.commenter.name,
					authorImageURL: item.commenter.image,
					postedDate: item.updated,
				}}
			/>
		);
	} else if (item.user) {
		// TODO: user
	} else {
		card = (
			<Card
				href={item.url || cardItemUrl}
				key={_s.slugify(item.title)}
				className="listItemCard"
				title={item.title}
				textLong={_s.prune(
					convertDescriptionToPlainText(item.description),
					240
				)}
				metadata={metadata}
			/>
		);
	}
	return card;
};

class List extends React.Component {
	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {
		const {
			loading,
			loadingComments,
			hasMoreToLoad,
			selectable,
			selectedItemIds = [],
			items,
			isPosts,
			isAdmin,
		} = this.props;

		if (loading) {
			return <ListLoadingItem />;
		} else if (loadingComments) {
			return <ListLoadingComment />;
		}

		const classes = ['list'];

		if (hasMoreToLoad) {
			classes.push('hasMoreToLoad');
		}

		return (
			<div className={classes.join(' ')}>
				{items.map(
					(item, i) =>
						item && (
							<div
								className={`listItem
								${selectable ? '-selectable' : ''} ${
									items.length === 1 ? '-listItemLengthOne' : ''
								}
								${selectedItemIds.indexOf(item._id) >= 0 ? '-selected' : ''}
								`}
								key={`${item.slug}-${i}`}
							>
								{selectable && (
									<div className="listItemSelector">
										<Checkbox
											checked={selectedItemIds.indexOf(item._id) >= 0}
										/>
									</div>
								)}
								{renderItem(item, isPosts, isAdmin)}
							</div>
						)
				)}
			</div>
		);
	}
}

List.propTypes = {
	hasMoreToLoad: PropTypes.bool,
	isAdmin: PropTypes.bool,
	isPosts: PropTypes.bool,
	items: PropTypes.array,
	loading: PropTypes.bool,
	loadingComments: PropTypes.bool,
	selectable: PropTypes.bool,
	selectedItemIds: PropTypes.arrayOf(PropTypes.string),
};

List.defaultProps = {
	items: [],
};

export default List;
