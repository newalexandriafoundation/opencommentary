/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

const PageMeta = props => {
  let title = `${props.pageTitle} — ${props.titleAppendix}`;

  if (props.noPageTitleAppendix) {
    title = props.pageTitle;
  }

  return (
    <Helmet>
      <title>{title}</title>
      <meta property="og:title" content={title} />
      <meta property="og:description" content={props.description} />
    </Helmet>
  );
};

PageMeta.propTypes = {
  noPageTitleAppendix: PropTypes.bool,
  pageTitle: PropTypes.string,
  titleAppendix: PropTypes.string,
};

PageMeta.defaultProps = {
  pageTitle: '',
  description: 'Open andAccessible Digital Philology for the Information Age',
  titleAppendix: 'Open Commentary Platform',
};

export default PageMeta;
