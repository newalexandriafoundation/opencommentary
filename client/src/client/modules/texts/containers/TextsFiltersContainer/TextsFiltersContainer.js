/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';

import Filters from '../../../../components/common/Filters';

class TextFiltersContainer extends React.Component {
	static propTypes = {
		filters: PropTypes.array,
		items: PropTypes.array,
		loading: PropTypes.bool,
	};

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.collection = JSON.parse(
			document.getElementById('collection').textContent
		);
	}

	render() {
		let filters = this.props.filters || [];
		let relevantFilters = [];

		let filterLookup = {
			language: {
				name: 'Language',
				type: 'text',
				values: [],
			},
			work_type: {
				name: 'Work Type',
				type: 'text',
				values: [],
			},
			structure: {
				name: 'Structure',
				type: 'text',
				values: [],
			},
			collection: {
				name: 'Collection',
				type: 'text',
				values: [],
			},
			textGroup: {
				name: 'Author',
				type: 'text',
				values: [],
			},
		};

		const collectionJSON =  JSON.parse(collection.textContent)

		// build filters
		const collections = collectionJSON.collections;
		collections.forEach(col =>{
			filterLookup.collection.values.push(col.title);
		});
		const textGroups = collectionJSON.text_groups;
		textGroups.forEach(tg => {
			filterLookup.textGroup.values.push(tg.title);
		});
		const languages = collectionJSON.languages;
		languages.forEach(lang => {
			filterLookup.language.values.push(lang.title);
		});

		if (this.props.items) {
			this.props.items.forEach(text => {
				text.type = 'text';
				let matchesFilters = true;

				text.works.forEach(work => {
					if (
						work.work_type &&
						work.work_type.length > 0 &&
						!filterLookup.work_type.values.includes(work.work_type)
					) {
						filterLookup.work_type.values.push(work.work_type);
					}

				});

			});
		}

		Object.keys(filterLookup).forEach(filter_name => {
			relevantFilters.push(filterLookup[filter_name]);
		});

		return ( 
			<div> {
				this.props.loading ? ( <
					Filters filters = {
						filters
					}
					loading / >
				) : ( <
					Filters filters = {
						filters
					}
					relevantFilters = {
						relevantFilters
					}
					/>
				)
			} </div>
		);
	}
}

export default TextFiltersContainer;
