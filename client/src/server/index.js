import 'dotenv/config';

import React from 'react';
import ReactDOMServer from 'react-dom/server';
import express from 'express';
import pino from 'pino-http';
import { ApolloProvider } from '@apollo/client';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/core/styles';
import { StaticRouter } from 'react-router';
import { renderToStringWithData } from "@apollo/react-ssr";

import customTheme from '#common/lib/muiTheme';
import Reading from '#common/modules/reading';
import { createApolloClient } from '#server/middleware/apolloClient';

const PORT = process.env.RENDERING_SERVER_PORT || 8082;

const app = express();
const logger = pino();

app.use(express.json({ limit: '200kb' }));
app.use(logger);

app.get('/heartbeat', (req, res) => {
	res.status(200);
	res.send('bm-bm bm-bm');
});

app.post('/read/:urn', async (req, res) => {
	const {
		// NOTE: (charles) not clear that collection is needed,
		// but leaving it here for now
		collection,
		project,
		USER_IS_ADMIN,
		USER_IS_EDITOR,
		USER_IS_COMMENTER,
	} = req.body;

	global.USER_IS_ADMIN = USER_IS_ADMIN;
	global.USER_IS_EDITOR = USER_IS_EDITOR;
	global.USER_IS_COMMENTER = USER_IS_COMMENTER;

	const apolloClient = createApolloClient(req);
	const context = {};
	// https://material-ui.com/guides/server-rendering/
	const sheets = new ServerStyleSheets();

	const App = sheets.collect(
		<ThemeProvider theme={customTheme}>
			<ApolloProvider client={apolloClient}>
			  <StaticRouter basename="/read" context={context} location={req.url}>
			  	<Reading project={project} />
			  </StaticRouter>
			</ApolloProvider>
		</ThemeProvider>
	);

	const content = await renderToStringWithData(App);
	const initialState = apolloClient.extract();
	const html = `
		<div id="root">${content}</div>
		<script>
			window.__APOLLO_STATE__=${JSON.stringify(initialState).replace(/</g, '\\u003c')};
		</script>
	`;

	res.status(200);
	res.json({ css: sheets.toString(), html, });
});

app.listen(PORT, () => {
	console.log(`Rendering server is ready and listening on port ${PORT}`);
});
