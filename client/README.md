# Alexandria React Frontend

[![pipeline status](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/badges/develop/pipeline.svg)](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/commits/develop)
[![coverage report](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/badges/develop/coverage.svg)](http://gitlab.archimedes.digital/archimedes/orpheus-fe-monorepo/commits/develop)

The project consists of 2 applications:

1.  React / Redux frontend application based on [`create-react-app`](https://github.com/facebookincubator/create-react-app) in this [repository](/packages/orpheus)

2.  GraphQL API (Node.js / Express) here: http://gitlab.archimedes.digital/archimedes/orpheus-api/tree/master

## Requirements

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

## Starting the project

### Modify hosts file on your local machine

On your local machine, add the following to your hosts file (on *nix at `/etc/hosts`):

```
127.0.0.1   alexandria.local
127.0.0.1   [project_name].alexandria.local
```

Repeat adding projects with their project name to the hosts file for the projects you'd like to create and work on locally.


### Start the React frontend

To start the project you need to follow these steps:

1.  Clone this repository

2.  Install dependencies

  ```sh
  $ yarn
  ```

3.  Set up environment variables:

  Configure these environment variables as necessary for your development environment so that the frontned knows which URL to access the GraphQL API backend at and which other routes to use for the authentication information.

  Environment variables for the frontend:

  ```sh
  $ cp .env.example .env
  # or copy these values into your own .env
  REACT_APP_GRAPHQL_URL=http://api.orpheus.local:3001
  REACT_APP_GRAPHQL_URI=graphql
  REACT_APP_SERVER=http://api.orpheus.local:3001
  REACT_APP_LOGIN_URI=auth/login
  REACT_APP_LOGOUT_URI=auth/logout
  REACT_APP_REGISTER_URI=auth/register
  REACT_APP_VERIFY_TOKEN_URI=auth/verify-token
  REACT_APP_COOKIE_DOMAIN=mindthegap.orpheus.local
  REACT_APP_BUCKET_URL=https://s3.amazonaws.com/iiif-orpheus
  REACT_APP_FACBOOK_CLIENT_ID=client_id
  REACT_APP_GOOGLE_CLIENT_ID=client_id
  REACT_APP_TWITTER_CLIENT_ID=client_id
  REACT_APP_GOOGLE_MAPS_API_KEY=mapkey
  ```

  Variables for using local api:
  ```sh
  REACT_APP_GRAPHQL_URL=http://api.orpheus.local:3001
  REACT_APP_SERVER=http://api.orpheus.local:3001
  REACT_APP_COOKIE_DOMAIN=.orpheus.local
  ```

  Variables for using staging api:
  ```sh
  REACT_APP_GRAPHQL_URL=http://api.staging.orphe.us
  REACT_APP_SERVER=http://api.staging.orphe.us
  REACT_APP_COOKIE_DOMAIN=.staging.orpheus.local
  ```

  Variables for using production api:
  ```sh
  REACT_APP_GRAPHQL_URL=http://api.orphe.us
  REACT_APP_SERVER=http://api.orphe.us
  REACT_APP_COOKIE_DOMAIN=.orpheus.local
  ```

4.  Start the engines.

  After configuring the environment variables and installing packages, you can start the application with
  ```sh
  $ yarn start
  ```
**!IMPORTANT** do NOT commit `.env` files to the repository. These should be used for __personal configuration__ and __secret values__.

## Starting Storybook (component library)

Follow the instructions above, but instead of `yarn start`, run `yarn run storybook`.
