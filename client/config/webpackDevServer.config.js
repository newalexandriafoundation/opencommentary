'use strict';

const path = require('path');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const evalSourceMapMiddleware = require('react-dev-utils/evalSourceMapMiddleware');
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');
const ignoredFiles = require('react-dev-utils/ignoredFiles');
const paths = require('./paths');
const fs = require('fs');

const protocol = process.env.HTTPS === 'true' ? 'https' : 'http';
const host = process.env.HOST || '0.0.0.0';

module.exports = function(proxy) {
	return {
		compress: true,
		hot: true,
		https: protocol === 'https',
		host,
		port: process.env.PORT || 3000,
		headers: {
			'Access-Control-Allow-Origin': '*',
		},
    allowedHosts: ['.alexandria.local'],
		historyApiFallback: {
			// Paths with dots should still use the history fallback.
			// See https://github.com/facebook/create-react-app/issues/387.
			disableDotRule: true,
		},
		// set WEBPACK_DEV_SERVER_BROWSER in your local .env if
		// you want webpack to open it right away
		open: process.env.WEBPACK_DEV_SERVER_BROWSER || false,
		proxy,
		static: {
			publicPath: '/'
		},
	};
};
