/**
 * @prettier
 */

const _knex = require('knex');
const _bookshelf = require('bookshelf');
const jsdom = require('jsdom');
const { ContentState, convertFromHTML, convertToRaw } = require('draft-js');

const knex = _knex({
	client: 'pg',
	connection: {
		database: process.env.DATABASE_NAME || 'alexandria',
		host: process.env.DATABASE_HOST || '0.0.0.0',
		password: process.env.DATABASE_PASSWORD || 'postgres',
		user: process.env.DATABASE_USER || 'postgres',
	},
});
const bookshelf = _bookshelf(knex)

const pageSize = 100;

// https://github.com/facebook/draft-js/issues/586#issuecomment-300347678
// which was inspired by https://github.com/facebook/draft-js/blob/master/src/model/paste/getSafeBodyFromHTML.js
const simpleDOMBuilder = html => {
	const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
	const _window = dom.window;

	global.document = _window.document;
	global.HTMLElement = _window.HTMLElement;
	global.HTMLAnchorElement = _window.HTMLAnchorElement;
	global.Node = _window.Node;

  const doc = document.implementation.createHTMLDocument('mydoc');
  doc.documentElement.innerHTML = html;
  const root = doc.getElementsByTagName('body')[0];
  return root;
};

const RevisionBase = bookshelf.model('RevisionBase', {
	tableName: 'alexandria_app_revisionbase',
});

function createContentStateFromLegacyText(text) {
	const { contentBlocks, entityMap } = convertFromHTML(
		text,
		simpleDOMBuilder
	);
	const baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);

	return convertToRaw(baseContent);
}

async function fetchRevisions(page) {
	console.log(`Fetching page ${page} of revisions.`);

	const results = await new RevisionBase().fetchPage({ page, pageSize });

	return results.models;
}

async function migrateText(revision) {
	const text = revision.get('text');

	try {
		JSON.parse(text);

		console.log(`Got JSON, skipping revision with id ${revision.get('id')}`);

		return Promise.resolve();
	} catch (e) {
		console.log('Failed to parse JSON, assuming HTML');

		const rawContentState = createContentStateFromLegacyText(text);

		return revision.set('text', JSON.stringify(rawContentState)).save();

		console.log(`Updated text to raw ContentState for revision with id ${revision.get('id')}`);
	}

	return Promise.resolve();
}

async function processRevisions(models) {
	return Promise.all(models.map(async model => {
		return await migrateText(model);
	}));
}

async function main() {
	const results = await new RevisionBase().fetchPage({ pageSize });
	const { models, pagination } = results;
	const { pageCount } = pagination;

	console.log(`Got ${pageCount} pages of revisions.`);

	await processRevisions(models);


	const promises = [];
	for (let i = 2; i <= pageCount; i++) {
		const revisions = await fetchRevisions(i)

		promises.push(processRevisions(revisions));
	}

	await Promise.all(promises);

	process.exit(0);
}

main();
