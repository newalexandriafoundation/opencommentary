const isDev = process.env.NODE_ENV === 'development';

const interpreter = isDev ? 'babel-node' : 'node';
const name = isDev ? 'ssr-development' : 'ssr';
const script = isDev ? 'src/server/index.js' : 'build/ssr/server/index.js';
const watch = isDev && ['src/common', 'src/server'];

module.exports = {
  apps : [{
  	interpreter,
  	name,
    script,
    watch,
  }],

  // deploy : {
  //   production : {
  //     user : 'SSH_USERNAME',
  //     host : 'SSH_HOSTMACHINE',
  //     ref  : 'origin/master',
  //     repo : 'GIT_REPOSITORY',
  //     path : 'DESTINATION_PATH',
  //     'pre-deploy-local': '',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
  //     'pre-setup': ''
  //   }
  // }
};
