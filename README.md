Open Commentary Platform
------

[![pipeline status](https://gitlab.com/newalexandriafoundation/opencommentary/badges/main/pipeline.svg)](https://gitlab.com/newalexandriafoundation/opencommentary/commits/main)
[![coverage report](https://gitlab.com/newalexandriafoundation/opencommentary/badges/main/coverage.svg)](https://gitlab.com/newalexandriafoundation/opencommentary/commits/main)

Open and Accessible Philology for the Information Age. The Open Commentary Platform project enables users to publish open and collaborative digital editions, commentaries, and translations for pre-medieval works in 14 languages, reusing texts from the Perseus Project, Open Greek and Latin, and CLTK.

# Development

## Prerequisites

* Docker
* docker-compose
* PostgreSQL

## Initial setup

### Setting up your database

Grab a database dump from a coworker and load it into your local PostgreSQL
instance. (Maybe we'll dockerize this someday.)

### Starting the application(s)

This application is designed to be run with `docker-compose` so that
development mirrors production as closely as possible. It is
meant to use the docker-compose.yml inside the `alexandria/` directory.

If you are starting from scratch, you will just need to modify your .env and
nginx.conf files to provide the credentials you want to use. It's probably
easiest if you keep things mostly the same as they are in the examples:

```
cp .env.example .env
cp config/settings_local.example.py config/settings_local.py
```

If you don't have a local version of the GraphQL Textserver running, make sure
that you adjust `TEXTSERVER_URL` to point to the remote version that you want
to use. You'll also need to make sure that `HOSTNAME` is properly configured as
an alias to `localhost` or `0.0.0.0` in your /etc/hosts file.

Once you have your environment variables and Django settings configured, simply run

``` sh
docker compose -f docker-compose.yml -f docker-compose.dev.yml up
```

to start the Django development server, the SSR development server, and the
Webpack Dev Server.

## Setting up the textserver

You might need to adjust the variables that point to the textserver. As of
right now, these are `TEXTSERVER_URL`, which should be defined from the
container context (i.e., using `host.docker.internal` to point to the host
machine's `localhost`); and `REACT_APP_GRAPHQL_URL`, which should be defined
from the host context (i.e., just `localhost`). If you're using a remote
server, you can use the same URL in both cases.

At some point we might want to consider making the textserver a part of this
Docker cluster, but it's a fairly heavy application for local development.

## Hosts file for multitenancy

Ideally, multitenancy should be handled through the nginx.conf.

To develop with multitenancy features, please add these entries to your the hosts file on your development machine, such as at `/etc/hosts`:

```
127.0.0.1       alexandria.local
127.0.0.1       homer.alexandria.local
127.0.0.1       test.alexandria.local
127.0.0.1       [other-subdomains-you-want-to-test].alexandria.local
```

You will want to configure the nginx.conf for handling these.

## Environment variables

Copy the `.env.example` file in the client directory to `.env` to set environment variables for the React frontend. To connect to different versions of the API for development (i.e. production or staging), modify the `REACT_APP_GRAPHQL_SERVER` variable, for instance.

The `.env.example` will work with a fresh clone to get you started developing with the staging API.

`docker compose` will automatically pick up environment variables in a .env
file in the same directory, but they will still need to be assigned to
variables for each container. See the docker-compose.yml file for examples.

# Production

CI/CD should take care of building/deploying to production most of the time, but
should you need to intervene manually, simply pull the latest build and run

``` sh
docker compose build
docker compose up
```

