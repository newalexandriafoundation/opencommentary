#!/bin/sh

# run db migrations
python manage.py migrate

# gunicorn
gunicorn --worker-tmp-dir /dev/shm config.wsgi
# gunicorn -b :8000 --log-level debug --timeout 300 -w 8 config.wsgi
