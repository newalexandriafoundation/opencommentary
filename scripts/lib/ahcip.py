import os
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport

ALEX_HOSTNAME_TO_AHCIP_TENANT_ID = {
    'homer': "4tJPty4gis3AhGt6z",
    'pindar': "km6CCNmQF7WEg7nce",
    'pausanias': "5Ns98kBwQAvi6sEyK",
}

def get_work_title_and_passages_from_legacy_comment_citation_urn(ahcip_comment):
    # eg. urn:cts:CHS:Commentaries.AHCIP:Odyssey.13.256-13.286.DFrM8TP
    # expect to return: (Odyssey, 13.256-13.286)
    legacy_comment_citation_urn = ahcip_comment['urn']['v2']
    urn_array = legacy_comment_citation_urn.split(':')
    work_array = urn_array[4].split('.')
    work_title = work_array[0]
    work_passage = '.'.join(work_array[1:-1])
    if ahcip_comment['section']:
        section_n = ahcip_comment['section']
        if len(work_array) > 4:
            work_passage = f'{work_array[1]}.{section_n}.{work_array[2]}.{section_n}.{work_array[3]}'
        else:
            work_passage = f'{work_array[1]}.{section_n}.{work_array[2]}'
    return work_title, work_passage

class Ahcip(object):
    def __init__(self):
        self.client = Client(
            transport=RequestsHTTPTransport(
                url=os.environ['AHCIP_API'] if 'AHCIP_API' in os.environ else 'http://api.chs.harvard.edu/graphql')
        )

    def fetch_comments(self, limit=1, skip=0, tenant_name='homer'):
        query_string = '''
            {
                comments(
                    queryParam:"{ \\"tenantId\\": \\"%s\\" }"
                    limit: %s
                    skip: %s
                ) {
                    _id
                    urn {
                        v2
                    }
                    subwork
                    section
                    status
                    tenantId
                    keywords {
                        title
                        tenantId
                    }
                    revisions {
                        title
                        text
                        textRaw
                        created
                    }
                    commenters {
                        tenantId
                        name
                    }
                    lemmaCitation {
                        ctsNamespace
                        textGroup
                        work
                        edition
                        subreferenceIndexFrom
                        subreferenceIndexTo
                    }
                }
            }
        ''' % ( ALEX_HOSTNAME_TO_AHCIP_TENANT_ID[tenant_name], limit, skip )
        query = gql(query_string)
        print("[INFO] GQL to AHCIP - ", query_string)
        return self.client.execute(query)

