"""A set of functions for parsing urn
"""


def parse_urn(urn, format="text_group"):
    """parse a string format urn into another format in string

    Arguments:
        urn {str} -- the input string format urn

    Keyword Arguments:
        format {str} -- a list of output formats to choose from (default: {"text_group"})

    Returns:
        str -- string output of the urn in target format
    """
    urn_parsed = urn
    urn_array = urn.split(':')
    if len(urn_array) < 2:
        return False
    if format == "text_group":
        urn_text_group = urn_array[3]
        urn_text_group_array = urn_text_group.split('.')
        # compose in format eg. urn:cts:greekLit:tlg0012.tlg001
        return f'{urn_array[0]}:{urn_array[1]}:{urn_array[2]}:{urn_text_group_array[0]}.{urn_text_group_array[1]}'
    elif format == "work":
        urn_text_group = urn_array[3]
        urn_text_group_array = urn_text_group.split('.')
        if len(urn_text_group_array) == 2:
            # this means that a sub-work wasn't provided, so we need to avoid an IndexError by not trying to get it
            # the result is the same as format == 'text_group'
            return f'{urn_array[0]}:{urn_array[1]}:{urn_array[2]}:{urn_text_group_array[0]}.{urn_text_group_array[1]}'
        else:
            # compose in format eg. urn:cts:greekLit:tlg0012.tlg001.perseus-grc2
            return f'{urn_array[0]}:{urn_array[1]}:{urn_array[2]}:{urn_text_group_array[0]}.{urn_text_group_array[1]}.{urn_text_group_array[2]}'
    elif format == "passages":
        # only returns the passage from to part
        # if len(urn_array) < 5:
            # return ''
        return f'{urn_array[4]}'
    return urn_parsed


def convert_passage_to_int(p):
    return int(p.split('@')[0])


def get_passages_from_urn(urn):
    urn_array = urn.split(':')
    if len(urn_array) <= 4:
        return {
            'from': None,
            'to': None,
        }
    passages_array = urn_array[4].split('-')
    passage_from = list(
        map(convert_passage_to_int, passages_array[0].split('.')))
    passages = {
        'from': passage_from,
        'to': list(
            map(
                convert_passage_to_int,
                passages_array[1].split('.')
            )
        ) if len(passages_array) > 1 else passage_from,
    }
    return passages


def is_passage_in_range(passage, target_passage):
    # whole text
    if not target_passage['from']:
        return True

    # diff book
    if passage['from'][0] != target_passage['from'][0]:
        # {'from': [5], 'to': [5]}, {'from': [1], 'to': [30]}
        if passage['from'][0] == passage['to'][0]:
            if target_passage['from'][0] <= passage['from'][0] and \
               passage['from'][0] <= target_passage['to'][0]:
                return True
        # 24.545 {'from': ['18', '115'], 'to': ['18', '144']}
        if passage['from'][0] > target_passage['to'][0]:
            return False
        # 17.522 {'from': ['18', '115'], 'to': ['18', '144']}
        if passage['from'][0] < target_passage['to'][0]:
            return False

    # same book
    if passage['from'][0] == target_passage['from'][0]:
        # empty to
        if target_passage['to'] and not passage['to']:
            #         18.115 ... 18.144
            # 18.1 == 18.115 == 18.144 != 18.145...
            if passage['from'][1] <= target_passage['to'][1]:
                return True
            else:
                return False
        # 18.85-18.87 {'from': ['18', '115'], 'to': ['18', '144']}
        if len(passage['to']) > 1 and \
           passage['to'][1] < target_passage['from'][1]:
            return False
        # {'from': [18, 235], 'to': [18, 240]}
        # {'from': [18, 115], 'to': [18, 144]}
        if len(passage['from']) > 1 and \
           passage['from'][1] > target_passage['to'][1]:
            return False

    # debug
    return True


def target_from_precedes_passage(target_from=[], passage_location=[]):
    if len(target_from) == 0:
        return True

    if len(target_from) == 1 and \
       len(passage_location) == 1 and \
       target_from[0] <= passage_location[0]:
        return True

    if target_from[0] <= passage_location[0]:
        return target_from_precedes_passage(
            target_from[1:],
            passage_location[1:])

    return False


def target_to_follows_passage(target_to, passage_location=[]):
    if len(target_to) == 0 or len(passage_location) == 0:
        return True

    if len(target_to) == 1 and \
       len(passage_location) == 1 and \
       target_to[0] >= passage_location[0]:
        return True

    if target_to[0] >= passage_location[0]:
        return target_to_follows_passage(target_to[1:], passage_location[1:])

    return False


def range_contains_passage(target_range, passage_from=[]):
    """
    A different approach to determining whether a passage
    is in range. Only cares about the starting point of the
    passage.
    """

    target_from = target_range.get('from')

    # on whole chapter
    if passage_from and \
       target_from and \
       len(passage_from) == 1 and \
       passage_from[0] == target_from[0]:
        return True

    if target_from is None or target_from == passage_from:
        return True

    target_to = target_range.get('to')
    if target_to == passage_from:
        return True
    elif target_to is None:
        target_to = target_from

    if not passage_from:
        return False

    # different books
    if target_from[0] < target_to[0]:
        if passage_from[0] >= target_from[0] and \
           passage_from[0] <= target_to[0]:
            return True

    return target_from_precedes_passage(target_from, passage_from) and \
        target_to_follows_passage(target_to, passage_from)


def urn_has_edition(urn):
    urn_array = urn.split(':')
    urn_text_group_array = urn_array[3].split('.')
    if len(urn_text_group_array) == 3:
        return True
    return False


def urn_comparator(user_text_a, user_text_b):
    passage_a = get_passages_from_urn(user_text_a.urn)['from']
    passage_b = get_passages_from_urn(user_text_b.urn)['from']
    depth = len(passage_a)
    for i in range(depth):
        if passage_a[i] < passage_b[i]:
            return -1
        elif passage_a[i] > passage_b[i]:
            return 1
        else:
            if i == (depth - 1):
                return 0
            else:
                continue
