from .textserver import Textserver

def get_archive_filters(project):
    text_fetcher = Textserver()
    results = text_fetcher.fetch_search_facets()
    collections = {'label': 'Collection', 'values': [{
        'id': c['id'],
        'slug': c['slug'],
        'title': c['title'],
        'urn': c['urn'],
    } for c in results['collections']]}
    languages = {'label': 'Language', 'values': results['languages']}
    authors = {
        'label': 'authors',
        'values': [tg for c in results['collections']
                   for tg in c['textGroups']]
    }

    filters = [authors, collections, languages]

    return filters
