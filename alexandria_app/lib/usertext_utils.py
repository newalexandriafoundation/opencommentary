from .parse_urn import parse_urn, get_passages_from_urn, is_passage_in_range
from ..models import UserText

from pprint import pprint


def get_usertexts_from_urn(urn, queryset=UserText.objects):
    # filter by textgroup
    text_group = parse_urn(urn)
    query_passage_range = get_passages_from_urn(urn)
    result_usertexts = []
    text_group_usertexts = queryset.filter(urn__contains=text_group)
    # filter with passage range

    for usertext in text_group_usertexts:
        if is_passage_in_range(get_passages_from_urn(usertext.urn), query_passage_range):
            result_usertexts.append(usertext)
    return result_usertexts
