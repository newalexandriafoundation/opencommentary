function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

(function ($) {
$( document ).ready(function() {

  // init headroom
  var headerEl = document.querySelector("header");
  if (headerEl) {
    var headroom  = new Headroom(headerEl);
    headroom.init();
  }

  // initial page scroll state
  if (window.pageYOffset > 0) {
    headerEl.classList.add('headroom--not-top');
  }

  // hide commentary section of sign up form on default
  $("#commentary_form").hide();
  $(document).on('click', '#create_commentary' , function() {
    if (this.checked){
        $("#commentary_form").show();
    }
    else {
        $("#commentary_form").hide();
    }
  });

  $('.toggleDescriptionButton').on('click', function(e) {
    $('.shortDescription').toggle();
    $('.longDescription').toggle();
  });

  $('.headerExternalButtonSearch').on('click', function(e) {
    $('header').addClass('-showSearch');
    $('.headerExternalSearch').removeClass('-hidden');
    $('#header_external_search').focus();
  });

  $('#header_external_search_back').on('click', function(e) {
    $('header').removeClass('-showSearch');
    $('.headerExternalSearch').addClass('-hidden');
    if (window.location.pathname.indexOf('/search/') === 0) {
      window.history.back();
    }
  });

  if (window.location.pathname.indexOf('/search/') === 0) {
    $('header').addClass('-onSearchRoute');
    $('.headerExternalSearch').removeClass('-hidden');
  }

  $('.headerExternalSearchBackground').on('click', function(e) {
    $('header').removeClass('-showSearch');
    $('.headerExternalSearch').addClass('-hidden');
  });

  $(document).keyup(function(e) {
    if (e.key === "Escape") {
      $('header').removeClass('-showSearch');
      $('.headerExternalSearch').addClass('-hidden');
    }
  });

  $('#search_form').submit(function(e) {
    e.preventDefault();
    window.location.replace(`/search/?s=${$('#header_external_search').val()}`);
  });

  /**
  var textSearchVal = getQueryVariable('s');
  if (textSearchVal && textSearchVal.length) {
    $('#header_external_search').val(textSearchVal);
  }
  */

  $('.helpButton').on("click", () => {
    window.$crisp = [];
    window.CRISP_WEBSITE_ID = "a791e061-d1c6-411d-8d0f-db8857b250a0";
    (function() {
      d = document;
      s = d.createElement("script");
      s.src = "https://client.crisp.chat/l.js";
      s.async = 1;
      d.getElementsByTagName("head")[0].appendChild(s);
    })();
    $crisp.push(['do', 'chat:open']);
  });

  $('.filterPanelSummary').on('click', (e) => {
    let $filter = $(e.target).parents('.filter');
    $filter.toggleClass("-facetsShown");
  });

  $('.facet').on('click', (e) => {
    let $facet = $(e.target);
    if (!$facet.hasClass('facet')) {
      $facet = $facet.parents('.facet');
    }
    $facet.toggleClass("-toggled");

    const filter = $facet.data('filter');
    const value = $facet.data('value');
    // const query = parseQuery(window.location.search);
    // let removeFilter = false;
    //
    // if (filter in query) {
    //   const values = query[filter].split(',');
    //   if (values.indexOf(value) >= 0) {
    //     values.splice(values.indexOf(value), 1);
    //     if (!values.length) {
    //       removeFilter = true;
    //     }
    //   } else {
    //     values.push(value);
    //   }
    //
    //   if (removeFilter) {
    //     delete query[filter];
    //   } else {
    //     query[filter] = values;
    //   }
    //
    // } else {
    //   query[filter] = value;
    // }
    //
    // query.page = 1;
    // window.location = `${window.location.href.replace(window.location.search, '')}?${serializeQuery(query)}`;
  });

});
})(jQuery);
