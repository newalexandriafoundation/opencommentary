from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

from rest_framework import routers

from . import views
from .admin import dashboard
from .root_schema import root_schema

# register django restful views
router = routers.DefaultRouter()
router.register(r'comments', views.CommentViewSet, basename='comment')
router.register(r'usertexts', views.UserTextViewSet, basename='usertext')
router.register(r'projects', views.ProjectViewSet)
router.register(r'tags', views.TagViewSet, basename='tag')

urlpatterns = [
    path('sitemap.xml', views.sitemap),
    path('', views.index, name="index"),
    path('coming-soon/', views.coming_soon, name="coming_soon"),
    path('texts/', views.texts, name="texts_index"),
    path('bookmarks/', views.bookmarks),
    path('bookmarks/<int:bookmark_id>/', views.bookmarks_delete),

    # commentaries
    path('commentaries/', views.commentaries, name="commentaries_index"),

    # commenters
    path('commenters/', views.commenters, name="commenters"),
    path('commenters/<str:username>/', views.commenter, name="commenter"),

    # search
    path('search/', views.search, name="search"),
    path('search/<str:model>/', views.search, name="search"),

    # texts
    path('texts/all', views.texts_all, name="texts_all"),
    path('texts/upload', views.text_upload, name="text_upload"),
    path('texts/<int:id>/', views.text, name="text_detail"),
    path('texts/<str:text_group_urn>', views.text_group, name="text_group_detail"),
    path('texts/all/', views.texts_all, name="texts_all"),
    # path('texts/all/<int:page>', views.texts_all, name="texts_all"),

    # tags
    path('tags/<int:id>/<str:fake_slug>', views.tag, name="tag"),

    # comments
    path('comments/<int:id>/<str:fake_slug>', views.comment, name="comment"),


    # reading
    path('read/<str:urn>', views.reading, name="read"),

    # pages
    path('about/', views.about_page, name="about"),
    path('thinking-comparatively-about-greek-mythology-x-a-homeric-lens-for-viewing-herakles/', views.example_post),

    # experimental generic views
    path('dashboard_exp/comments/', views.CommentListView.as_view()),
    path('dashboard_exp/', views.dashboard, name='dashboard'),
    path('dashboard/', dashboard.urls),
    # end of experimental

    # endpoints read from textserver
    path('textgroups/<str:urn>/', views.text_groups),
    path('work/<str:urn>/', views.work),

    # auth
    path('accounts/', include('django.contrib.auth.urls')),
    path('profile/', views.user_profile, name='profile'),
    path('sign-up/', views.sign_up, name='sign_up'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),

    # django restful
    path('api/', include(router.urls)),
    path('rest-auth/', include('rest_auth.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # misc
    path('sign_s3/', views.SignS3Upload.as_view()),
    path("graphql", csrf_exempt(GraphQLView.as_view(
        graphiql=True, schema=root_schema))),
    path('tinymce/', include('tinymce.urls')),
]
