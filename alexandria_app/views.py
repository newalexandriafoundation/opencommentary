import re
import boto3
import json
import logging
import rules
import requests
from functools import cmp_to_key

from botocore.client import Config
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core import serializers
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.db.models import IntegerField, Q, F
from django.utils.html import strip_tags
from django_sitemaps import Sitemap

# experimental
from django.views.generic.list import ListView
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from slugify import slugify

from .forms import CustomUserCreationForm, ProjectForm, TextForm
from .lib.comment_utils import get_comments_from_urn, get_commentaries_from_urn, get_counts_by_urn, \
    get_counts_for_work_by_urn, get_texts_from_comments_urns
from .lib.default_text_images import DEFAULT_TEXT_IMAGES
from .lib.get_archive_filters import get_archive_filters
from .lib.get_archive_hostname import get_archive_hostname
from .lib.get_project_from_request import get_project_from_request
from .lib.highlight_search_term import highlight_search_term
from .lib.parse_urn import parse_urn, urn_comparator
from .lib.project_content_fetcher import get_project_commenters, get_relevant_works, get_all_works, fetch_text_meta
from .lib.render_react_view import render_react_view
from .lib.search_source_text_for_commentary import search_source_text_for_commentary
from .lib.set_active_filters import set_active_filters
from .lib.textserver import Textserver
from .lib.usertext_utils import get_usertexts_from_urn
from .lib.utils import get_url_with_modified_subdomain, pick_a_text
from .models import Project, Comment, Bookmark, UserText, Tag, TextMeta, CustomUser
from .serializers import CommentSerializer, TagSerializer, UserTextSerializer, ProjectSerializer
from .helpers import request_passes_test, user_can_view_commentary

from pprint import pprint
#from time import time

# end of experimental

logger = logging.getLogger('alexandria_app')
logger.setLevel(logging.DEBUG if settings.DEBUG else logging.INFO)

# Create your views here.
s3 = boto3.client(
    's3',
    region_name='us-east-2',
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
    config=Config(signature_version='s3v4'),
)


@request_passes_test(user_can_view_commentary)
def index(request):
    # *.alexandria.domain
    hostname = get_archive_hostname(request)

    logger.info("Hostname: %s" % hostname)

    if len(hostname) and hostname != 'oc':
        project = Project.objects.get(hostname=hostname)
        comment_list = Comment.objects.filter(project_id=project.id)
        project_commenters = get_project_commenters(comment_list, hostname)
        (relevant_works, read_urn) = get_relevant_works(project)

        if hostname == 'pindar':
            read_urn = 'urn:cts:greekLit:tlg0033.tlg001.perseus-grc2'
        elif hostname == 'pausanias':
            read_urn = 'urn:cts:greekLit:tlg0525.tlg001.perseus-grc2'
        elif hostname == 'homer':
            read_urn = 'urn:cts:greekLit:tlg0012.tlg001.perseus-grc2'

        return render(request, 'commentary.twig', {
            'project': project,
            'project_commenters': project_commenters,
            'project_comments': comment_list,
            'project_texts': relevant_works['works'],
            'read_urn': read_urn,
            'aws_url': settings.AWS_MEDIA_URL,
        })

    # alexandria.domain
    return render(request, 'index.twig')


def coming_soon(request):
    return render(request, 'coming_soon.twig', {
        'body_class': " ".join(["page", "comingSoonPage"])
    })


def search(request, model="all"):
    hostname = get_archive_hostname(request)
    project = Project.objects.get(hostname=hostname)
    search_term = ''
    total_count = 0

    # get commenter search facets
    comment_list = Comment.objects.filter(project_id=project.id)
    project_commenters = get_project_commenters(comment_list)
    commenter_search_values = []
    for commenter in project_commenters:
        commenter_search_values.append({
                'value': commenter.full_name,
            })

    # get tag search facets
    tags = Tag.objects.filter(project=project).all()
    tag_search_values = []
    for tag in tags:
        tag_search_values.append({
            'value': tag.name,
        })

    tag_results = []
    comment_results = []
    source_text_results = []
    translation_results = []

    # search comments, tags, and source text / translation
    comments = Comment.objects.filter(project=project)
    tags = Tag.objects.filter(project=project)

    # add search filters
    for q in request.GET:
        if q in ['page']:
            continue

        label = q
        value = request.GET[q]

        if label == 's':
            search_term = value
            # search comments
            comments = comments.filter(Q(revisions__text__icontains=value) | Q(revisions__title__icontains=value)).distinct()
            # search tags
            tags = tags.filter(name__icontains=value)

    # search source text & translations
    source_text_nodes, translation_text_nodes = search_source_text_for_commentary(project, search_term)

    # process comment results
    for comment in comments:
        title = ''
        if comment.revisions.first().title and len(comment.revisions.first().title):
            title = comment.revisions.first().title
        else:
            title = comment.urn
        byline = ''
        commenter_names = []
        for commenter in comment.commenters.all():
            commenter_names.append(commenter.full_name)
        byline = ", ".join(commenter_names)
        revisions_overall = ''
        for revision in comment.revisions.reverse():
            revisions_overall += revision.text

        body_text = revisions_overall
        excerpt = highlight_search_term(search_term, body_text)

        comment_results.append({
            'title': title,
            'byline': byline,
            'excerpt': excerpt,
            'url': "/comments/{}/{}".format(comment.id, slugify(comment.revisions.last().title))
        })

    # process tag results
    for tag in tags:

        tag_results.append({
            'id': tag.id,
            'name': tag.name,
            'image': tag.image,
            'count': tag.comments.all().count(),
        })

    # process source text and translation results
    for text_node in source_text_nodes:
        joined_location = ".".join(str(i) for i in text_node['location'])
        title = "{} {}".format(text_node['work']['english_title'], joined_location)

        source_text_results.append({
            'title': title,
            'excerpt': highlight_search_term(search_term, text_node['text']),
            'url': "/read/{}:{}".format(text_node['work']['full_urn'], joined_location),
        })
    for text_node in translation_text_nodes:
        joined_location = ".".join(str(i) for i in text_node['location'])
        title = "{} {}".format(text_node['work']['english_title'], joined_location)

        translation_results.append({
            'title': title,
            'excerpt': highlight_search_term(search_term, text_node['text']),
            'url': "/read/{}:{}".format(text_node['work']['full_urn'], joined_location),
        })

    tag_results_count = len(tag_results)
    comment_results_count = len(comment_results)
    source_text_count = len(source_text_results)
    translation_count = len(translation_results)

    total_count += tag_results_count
    total_count += comment_results_count
    total_count += source_text_count
    total_count += translation_count

    if model == "all":
        tag_results = tag_results[:21]
        comment_results = comment_results[:21]
        source_text_results = source_text_results[:21]
        translation_results = translation_results[:21]
    elif model == "commentary":
        tag_results = tag_results[:21]
        comment_results = comment_results
        source_text_results = source_text_results[:21]
        translation_results = translation_results[:21]

    return render(request, 'search.twig', {
        'body_class': " ".join(["page", "pageSearch"]),
        'header_theme': '-light',
        'search_term': search_term,
        'total_count': total_count,
        'tag_results_count': tag_results_count,
        'tag_results': tag_results,
        'comment_results_count': comment_results_count,
        'comment_results': comment_results,
        'source_text_results': source_text_results,
        'translation_results': translation_results,
        'model': model,
        'search_route': True,
        'querystring': request.GET.urlencode(),
        'aws_url': settings.AWS_MEDIA_URL,
    })


def texts(request):
    relevant_works = []
    hostname = get_archive_hostname(request)
    project = None
    if hostname is not None:
        project = Project.objects.get(hostname=hostname)
    (relevant_works, _) = get_relevant_works(project, request)

    return render_react_view(request, 'react_view.twig', {
        'body_class': " ".join(["page", "pageList", "pageListTexts"]),
        'header_theme': '-light',
        'component_name': 'TextsView',
        'include_header_and_footer': True,
        'collection_json': relevant_works,
    })


@request_passes_test(user_can_view_commentary)
def texts_all(request):
    collection, pagination = get_all_works(request)

    return render_react_view(request, 'react_view.twig', {
        'body_class': " ".join(["page", "pageList", "pageListTexts"]),
        'header_theme': '-light',
        'component_name': 'TextsView',
        'include_header_and_footer': True,
        'collection_json': collection,
        'pagination': pagination,
    })


@request_passes_test(user_can_view_commentary)
def text(request, id):
    # get text
    text_fetcher = Textserver()
    text_work = text_fetcher.fetch_work_by_id(id)['work']

    # get cover image
    cover_image = static(f"img/{DEFAULT_TEXT_IMAGES[0]}")
    try:
        text_meta = TextMeta.objects.get(urn=text_work['urn'])
        if text_meta.cover_image.url:
            cover_image = text_meta.cover_image.url
    except Exception as _err:
        text_meta = {}

    # get relevant commentaries
    commentaries = get_commentaries_from_urn(text_work['urn'])
    return render(request, 'text.twig', {
        'header_theme': '-light',
        'id': id,
        'body_class': " ".join(["page", "pageSingle", "pageSingleText"]),
        'text': text_work,
        'text_image': cover_image,
        'text_meta': text_meta,
        'commentaries': commentaries,
    })

def tag(request, id, fake_slug):
    hostname = get_archive_hostname(request)
    if len(hostname):
        project = Project.objects.get(hostname=hostname)
    tag = Tag.objects.get(id=id)
    comments = Comment.objects.filter(tags__id=tag.id, project_id=project.id)
    comment_results = []

    # process comment results
    for comment in comments:
        title = ''
        if comment.revisions.first().title and len(comment.revisions.first().title):
            title = comment.revisions.first().title
        else:
            title = comment.urn
        byline = ''
        commenter_names = []
        for commenter in comment.commenters.all():
            commenter_names.append(commenter.full_name)
        byline = ", ".join(commenter_names)
        revisions_overall = ''
        for revision in comment.revisions.reverse():
            revisions_overall += revision.text

        excerpt = highlight_search_term(tag.name, revisions_overall)

        comment_results.append({
            'title': title,
            'byline': byline,
            'excerpt': excerpt,
            'url': "/comments/{}/{}".format(comment.id, slugify(comment.revisions.last().title))
        })

    return render(request, 'tag.twig', {
        'header_theme': '-light',
        'id': id,
        'body_class': " ".join(["page", "pageSingle", "pageSingleText"]),
        'tag': tag,
        'comment_results': comment_results,
        'aws_url': settings.AWS_MEDIA_URL,
    })


def comment(request, id, fake_slug):
    comment = Comment.objects.get(id=id)

    return render(request, 'comment.twig', {
        'header_theme': '-light',
        'id': id,
        'body_class': " ".join(["page", "pageSingle", "pageSingleText"]),
        'comment': comment,
    })


@request_passes_test(user_can_view_commentary)
def text_upload(request):
    message = ''
    uploaded = False

    if request.method == 'POST':
        form = TextForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['text_file']
            text_file = {
                'filename': file.name,
                'content': file.read()
            }
            r = requests.post(settings.TEXTSERVER_TEXT, data=text_file)
            r_json = r.json()
            uploaded = r_json['ok']
            if not uploaded:
                message = r_json['error']
            else:
                message = 'Text uploaded. Please wait a few minutes for it to be ingested.'
    else:
        form = TextForm()

    context = {'form': form, 'message': message, 'uploaded': uploaded}
    return render(request, 'text_upload.twig', context)


def text_group(request, text_group_urn):

    # get texts
    text_fetcher = Textserver()
    works = text_fetcher.fetch_works(text_group_urn)

    # fetch text meta
    works = fetch_text_meta(works['works'])

    # one text
    text = pick_a_text(works)

    # get cover image
    cover_image = static(f"img/{DEFAULT_TEXT_IMAGES[0]}")
    try:
        text_meta = TextMeta.objects.get(urn=text_group_urn)
        if text_meta.cover_image.url:
            cover_image = text_meta.cover_image.url
    except Exception as _err:
        text_meta = {}

    # get relevant commentaries
    commentaries = get_commentaries_from_urn(text_group_urn)

    return render(request, 'texts.twig', {
        'header_theme': '-light',
        'id': id,
        'body_class': " ".join(["page", "pageSingle", "pageSingleText"]),
        'text': text,
        'texts': works,
        'text_image': cover_image,
        'text_meta': text_meta,
        'commentaries': commentaries,
    })


def commenters(request):
    # *.alexandria.domain
    hostname = get_archive_hostname(request)
    if len(hostname):
        project = Project.objects.get(hostname=hostname)
        comment_list = Comment.objects.filter(project_id=project.id)
        project_commenters = get_project_commenters(comment_list)

    return render(request, 'commenters.twig', {
        'header_theme': '-light',
        'id': id,
        'body_class': " ".join(["page", "pageSingle", "pageSingleText"]),
        'commenters': project_commenters,
        'aws_url': settings.AWS_MEDIA_URL,
    })


def commenter(request, username):
    # get commenter
    commenter = CustomUser.objects.get(username=username)

    # get commenter commentaries
    commentaries = Project.objects.filter(commenters__id=commenter.id)

    # get commenter comments
    comments_urns = Comment.objects.filter(
        commenters__id=commenter.id).values_list('urn', flat=True).distinct()
    works = get_texts_from_comments_urns(comments_urns)

    # featured comment quote
    comment_featured = Comment.objects.filter(featured=True).last()
    # pprint(comment_featured.revisions.last().text)

    return render(request, 'commenter.twig', {
        'texts': works['workSearch']['works'],
        'commentaries': commentaries,
        'commenter': commenter,
        'aws_url': settings.AWS_MEDIA_URL,
        'comment_featured': comment_featured,
        'header_theme': '-light',
    })


@login_required
def bookmarks(request):
    try:
        hostname = get_archive_hostname(request)
        project = Project.objects.get(hostname=hostname)

        if project:
            if request.method == 'GET':
                bs = Bookmark.objects.filter(
                    project=project, user=request.user)

                return JsonResponse({
                    'bookmarks': json.loads(serializers.serialize('json', bs))
                }, status=200)
            elif request.method == 'POST':
                body = json.loads(request.body)
                b = Bookmark.objects.create(
                    project=project,
                    user=request.user,
                    urn=body['urn'],
                    title=body.get('title'))

                return JsonResponse({'bookmark': {'id': b.id}}, status=201)
    except Exception as err:
        print(type(err))
        print(err.args)
        return HttpResponse(err, status=400)


@login_required
def bookmarks_delete(request, bookmark_id):
    try:
        hostname = get_archive_hostname(request)
        project = Project.objects.get(hostname=hostname)

        if project:
            b = Bookmark.objects.get(id=bookmark_id)

            if request.method == 'DELETE':
                b.delete()

                return JsonResponse({'bookmark': {'id': b.id}}, status=200)
            else:
                return JsonResponse({
                    'bookmark': json.loads(serializers.serialize('json', b))})
    except Exception as err:
        print(err.args)
        return HttpResponse(err, status=400)


def text_groups(request, urn):
    try:
        # get works from unique urns with comments
        text_fetcher = Textserver()
        text_groups = text_fetcher.fetch_textgroups(urn)
        return JsonResponse(text_groups, status=200)

    except Exception as err:
        print(type(err))
        print(err.args)
        return HttpResponse(err, status=400)


def work(request, urn):
    try:
        # get works from unique urns with comments
        text_fetcher = Textserver()
        work = text_fetcher.fetch_works_by_urn(urn)
        return JsonResponse(work, status=200)

    except Exception as err:
        print(type(err))
        print(err.args)
        return HttpResponse(err, status=400)


def commentaries(request):

    commentary_dict_list = []
    commentaries = Project.objects.filter(privacy='PUBLIC')
    for commentary in commentaries:
        commentary_dict = {
            'title': commentary.title,
            'textShort': ','.join(
                [f'{commenter.first_name} {commenter.last_name}' for commenter in commentary.commenters.all()]),
            'description': commentary.subtitle,
            'type': 'commentary',
            'slug': slugify(commentary.hostname),
            'comments_count': Comment.objects.filter(project=commentary).count(),
            'files': [{
                'name': commentary.background_image.url if commentary.background_image else 'hector.jpg',
                'type': "image/jpeg",
            }],
            # 'originalLanguage': 'English',
            'tags': [tag.name for tag in Tag.objects.filter(project=commentary).all()],
            'authors': [f'{commenter.first_name} {commenter.last_name}' for commenter in commentary.commenters.all()],
        }
        commentary_dict_list.append(commentary_dict)

    return render_react_view(request, 'react_view.twig', {
        'body_class': " ".join(["page", "pageList", "pageListCommentaries"]),
        'header_theme': '-light',
        'component_name': 'CommentariesView',
        'include_header_and_footer': True,
        'commentaries_json': commentary_dict_list,
    })


def reading(request, urn):
    text_fetcher = Textserver()
    collection = text_fetcher.fetch_works_by_urn(parse_urn(urn, 'work'))
    hostname = get_archive_hostname(request)
    project = None
    if len(hostname):
        project = Project.objects.get(hostname=hostname)
    else:
        project = Project.objects.get(hostname='oc')

    return render_react_view(request, 'react_view.twig', {
        'collection': collection,
        'component_name': 'Main',
        'project': {
            'canonical_domain': project.canonical_domain,
            'id': project.id,
            'title': project.title,
            'hostname': project.hostname
        },
        'urn': urn,
        'include_header_and_footer': False,
    })


def about_page(request):
    about_html = ""
    hostname = get_archive_hostname(request)
    if len(hostname):
        project = Project.objects.get(hostname=hostname)
        about_html = project.about_html
    return render(request, 'page-about.twig', {
        'about_html': about_html,
        'aws_url': settings.AWS_MEDIA_URL,
    })


def example_post(request):

    return render(request, 'post.twig')


def page(request):

    return render(request, 'page.twig')


def not_found(request, exception):

    return render(request, '404.twig')

def sign_up(request):
    registered = False

    # perform registration
    if request.method == 'POST':
        custom_user = None
        custom_user_form = CustomUserCreationForm(data=request.POST)
        project_form = ProjectForm(data=request.POST)
        del project_form.fields['admins']
        del project_form.fields['editors']
        del project_form.fields['commenters']
        # save user
        if custom_user_form.is_valid():
            # create user
            custom_user = custom_user_form.save()
            if 'picture' in request.FILES:
                custom_user.picture = request.FILES['picture']
            custom_user.save()
            registered = True
        # when project form is provided
        if project_form['hostname'].value() and project_form.is_valid():
            # create project
            project = project_form.save()
            if project:
                project.admins.add(custom_user)
                project.save()
            # give user dashboard access and add user to
            # CommentaryStaff group for permissions on project and comments
            group = Group.objects.get(name='CommentaryStaff')
            custom_user.groups.add(group)
            custom_user.is_staff = True
            custom_user.save()
            # redirect to newly registered commentary
            return redirect(get_url_with_modified_subdomain(project.hostname, request.build_absolute_uri()))

    # show reg form
    else:
        custom_user_form = CustomUserCreationForm()
        project_form = ProjectForm()
        # hide role assignment on project creation
        project_form.fields['admins'].widget = forms.HiddenInput()
        project_form.fields['editors'].widget = forms.HiddenInput()
        project_form.fields['commenters'].widget = forms.HiddenInput()

    # render
    context = {
        'custom_user_form': custom_user_form,
        'project_form': project_form,
        'registered': registered
    }
    return render(request, 'sign_up.twig', context)


def user_login(request):
    # perform login
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                redirect_to = request.GET.get('redirect_to', None)

                # no idea why the failed get is being cast to a string
                # ^ update: I think it's because it *is* a string.
                # See login.twig
                if redirect_to is not None and redirect_to != 'None':
                    return HttpResponseRedirect(redirect_to)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your account is inactivate.")
        else:
            messages.error(request, "Your username or password is incorrect.")
            return HttpResponseRedirect(reverse('login'))

    # show login form
    else:
        # render
        return render(request, 'login.twig', { 'redirect_to': request.GET.get('redirect_to') })


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


@login_required
def user_profile(request):
    return redirect('/dashboard/alexandria_app/userprofile/')


@login_required
def dashboard(request):
    # access check
    if not rules.test_rule('can_access_commentary_dashboard', request.user, get_archive_hostname(request)):
        return render(request, 'dashboard-unauthorized.twig', {})

    saved = False

    # save project settings from dashboard form
    if request.method == 'POST':
        hostname = get_archive_hostname(request)
        project = get_object_or_404(Project, hostname=hostname)
        dashboard_form = ProjectForm(request.POST or None, instance=project)
        # save project
        if dashboard_form.is_valid():
            project_saved = dashboard_form.save()
            saved = True
        # invalid input
        else:
            print(dashboard_form.errors)

    # show edit Project form
    else:
        hostname = get_archive_hostname(request)
        project = get_object_or_404(Project, hostname=hostname)
        dashboard_form = ProjectForm(instance=project)

    # render
    context = {
        'dashboard_form': dashboard_form,
        'saved': saved
    }
    return render(request, 'dashboard.twig', context)

"""
custom sitemaps with access to request
"""
def sitemap(request):
    sitemap = Sitemap(
        build_absolute_uri=request.build_absolute_uri,
    )

    # static sitemaps

    # home
    sitemap.add(
        reverse('index'),
        changefreq='weekly',
        priority=0.5,
    )

    # about
    sitemap.add(
        reverse('about'),
        changefreq='monthly',
        priority=0.5,
    )

    # dynamic sitemaps
    project = get_project_from_request(request)

    # texts
    texts, _ = get_relevant_works(project)
    works = texts['works']
    for work in works:
        url = f"/texts/{work['text_group_urn']}"
        sitemap.add(
            url,
            changefreq='hourly',
            priority=0.8
        )

    # read/urn
    for comment in Comment.objects.filter(project=project).all():
        url = f"/read/{comment.urn}"
        sitemap.add(
            url,
            changefreq='hourly',
            priority=0.8,
            lastmod=comment.updated_at,
        )

    # comments
    for comment in Comment.objects.filter(project=project).all():
        fake_slug = "untitled"
        last_revision = comment.revisions.first()
        if last_revision and last_revision.title:
            fake_slug = slugify(last_revision.title)
        url = f"/comments/{comment.id}/{fake_slug}"
        sitemap.add(
            url,
            changefreq='hourly',
            priority=0.8,
            lastmod=comment.updated_at,
        )

    # commenters
    for commenter in project.commenters.all():
        url = f"/commenters/{commenter.username}"
        sitemap.add(
            url,
            changefreq='daily',
            priority=0.8,
            lastmod=project.updated_at,
        )

    # tags
    for tag in Tag.objects.filter(project=project).all():
        url = f"/tags/{tag.id}/{slugify(tag.name)}"
        sitemap.add(
            url,
            changefreq='daily',
            priority=0.8,
            lastmod=project.updated_at,
        )

    return sitemap.response(
        pretty_print=settings.DEBUG,
    )


class SignS3Upload(APIView):
    def get(self, request):

        s3_bucket = settings.AWS_STORAGE_BUCKET_NAME
        file_name = request.GET['file_name']

        presigned_post = s3.generate_presigned_post(
            Bucket=s3_bucket,
            Conditions=[{'acl': 'public-read'}],
            Fields={'acl': 'public-read'},
            Key=file_name
        )

        data = presigned_post
        return Response(data)


# experimental generic class based views
class CommentListView(LoginRequiredMixin, ListView):

    model = Comment
    paginate_by = 10
    template_name = 'comment_list.twig'

    def get_queryset(self):
        project = get_project_from_request(self.request)
        # TODO: role filters
        return Comment.objects.filter(project_id=project.id)

# django restful views

class CommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows comments to be viewed or edited.
    """
    serializer_class = CommentSerializer

    def get_queryset(self):
        # all
        queryset = Comment.objects
        # filter by hostname
        project = get_project_from_request(self.request)
        queryset = queryset.filter(project_id=project.id, privacy='PUBLIC')
        # filter on urn search term
        urn_search = self.request.query_params.get('urn_search', None)

        if urn_search is not None:
            return get_comments_from_urn(urn_search, queryset=queryset.order_by('urn'))
        return queryset.order_by('urn')

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        serializer.update(instance, request.data)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def counts_by_urn(self, request):
        urn_search = request.query_params.get('urn_search', None)

        if urn_search is None:
            return Response({ 'error': 'You must provide a urn_search param.' }, status=400)
        queryset = Comment.objects
        project = get_project_from_request(self.request)
        queryset = queryset.filter(project_id=project.id, privacy='PUBLIC')

        counts_by_urn = get_counts_for_work_by_urn(urn_search, queryset=queryset)

        return Response({ 'results': counts_by_urn })

    @action(detail=True, methods=['delete'])
    def remove_tag(self, request, pk=None):
        tag_name = request.query_params.get('name')

        if tag_name is None:
            return Response({ 'error': 'You must provide a tag name if you want to delete a tag.' }, status=400)

        comment = self.get_object()
        tag = Tag.objects.get(name=tag_name)

        comment.tags.remove(tag)
        tag.comments.remove(comment)

        return Response({ 'result': { 'comment': comment.id, 'tag': tag.name } })


class TagViewSet(viewsets.ModelViewSet):
    """
    API endpoint for tags
    """
    serializer_class = TagSerializer

    def get_queryset(self):
        # all
        queryset = Tag.objects
        # filter by hostname
        project = get_project_from_request(self.request)
        queryset = queryset.filter(project_id=project.id)
        # filter on ?q=<search_term>
        q = self.request.query_params.get('q', None)
        if q is not None:
            queryset = queryset.filter(name__icontains=q)
            return queryset
        return queryset.all()

    def create(self, request, *args, **kwargs):
        name = request.data.get('name', None)

        if name is None:
            return Response(status=400)

        project = get_project_from_request(request)
        tag = None

        try:
            tag = Tag.objects.get(name=name)
        except Tag.DoesNotExist:
            tag = Tag.objects.create(name=name, project=project)

        serializer = self.get_serializer(tag)

        return Response(serializer.data)


class UserTextViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows usertext to be viewed or edited.
    """
    serializer_class = UserTextSerializer

    def get_queryset(self):
        # all
        queryset = UserText.objects
        # filter by hostname
        project = get_project_from_request(self.request)
        queryset = queryset.filter(project_id=project.id)
        content_type = self.request.query_params.get('content_type', None)

        if content_type is not None:
            queryset = queryset.filter(content_type=content_type.upper())

        # filter on urn search term
        urn_search = self.request.query_params.get('urn_search', None)
        if urn_search is not None:
            search_result = get_usertexts_from_urn(urn_search, queryset=queryset)
            search_result_sorted = sorted(search_result, key=cmp_to_key(urn_comparator))
            return search_result_sorted
        return queryset.all()


class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows project to be viewed.
    """
    queryset = Project.objects.all().order_by('id')
    serializer_class = ProjectSerializer

    def retrieve(self, request, pk=None):
        project = get_project_from_request(request)

        if project is not None:
            serializer = self.get_serializer(project)

            return Response(serializer.data)

    @action(detail=True)
    def works(self, request, pk=None):
        project = get_object_or_404(self.queryset, pk=pk)
        (project_works, read_urn) = get_relevant_works(project, request)

        return Response({ 'results': project_works['works'] })
