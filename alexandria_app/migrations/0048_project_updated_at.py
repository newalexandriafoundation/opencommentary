# Generated by Django 2.2.6 on 2021-06-02 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alexandria_app', '0047_usertext_updated_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
