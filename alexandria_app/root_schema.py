import graphene

from .gql_schema import queries, mutations

class Query(
    queries.Query, 
    graphene.ObjectType
    ):
    pass

# disable mutations because GQL is only used as read-only endpoint for custom clients eg. AHCIP client
""" 
class Mutation(
    mutations.Mutation, 
    graphene.ObjectType
    ):
    pass
"""

root_schema = graphene.Schema(query=Query)