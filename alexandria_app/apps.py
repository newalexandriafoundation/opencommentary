from django.apps import AppConfig


class AlexandriaAppConfig(AppConfig):
    name = 'alexandria_app'
