import graphene

from alexandria_app.models import UserText, RevisionUserText, Comment, RevisionComment, Project

from .type.user_text import UserTextType
from .type.comment import CommentType


class UserTextCreateMutation(graphene.Mutation):
    class Arguments:
        project_id = graphene.ID()
        revision_text = graphene.String(required=True)

    user_text = graphene.Field(UserTextType)

    def mutate(self, info, project_id, revision_text):
        project = Project.objects.get(pk=project_id)
        user_text = UserText(
            project=project
        )
        user_text.save()
        new_revision = RevisionUserText(
            user_text=user_text,
            text=revision_text,
        )
        new_revision.save(force_insert=True)
        return UserTextCreateMutation(user_text=user_text)

class UserTextUpdateMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        revision_text = graphene.String(required=True)

    user_text = graphene.Field(UserTextType)

    def mutate(self, info, id, revision_text):
        user_text = UserText.objects.get(pk=id)
        new_revision = RevisionUserText(
            user_text=user_text,
            text=revision_text,
        )
        user_text.save()
        new_revision.save(force_insert=True)
        return UserTextUpdateMutation(user_text=user_text)

class UserTextDeleteMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    ok = graphene.Boolean()

    @classmethod
    def mutate(cls, root, info, **kwargs):
        user_text = UserText.objects.get(pk=kwargs["id"])
        user_text.delete()
        return cls(ok=True)

class CommentCreateMutation(graphene.Mutation):
    class Arguments:
        project_id = graphene.ID()
        revision_text = graphene.String(required=True)

    comment = graphene.Field(CommentType)

    def mutate(self, info, project_id, revision_text):
        project = Project.objects.get(pk=project_id)
        comment = Comment(
            project=project
        )
        comment.save()
        new_revision = RevisionComment(
            comment=comment,
            text=revision_text,
        )
        new_revision.save(force_insert=True)
        return CommentCreateMutation(comment=comment)

class CommentUpdateMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        revision_text = graphene.String(required=True)

    comment = graphene.Field(CommentType)

    def mutate(self, info, id, revision_text):
        comment = Comment.objects.get(pk=id)
        new_revision = RevisionComment(
            comment=comment,
            text=revision_text,
        )
        comment.save()
        new_revision.save(force_insert=True)
        return CommentUpdateMutation(comment=comment)

class CommentDeleteMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    ok = graphene.Boolean()

    @classmethod
    def mutate(cls, root, info, **kwargs):
        comment = Comment.objects.get(pk=kwargs["id"])
        comment.delete()
        return cls(ok=True)

class Mutation(graphene.ObjectType):
    create_user_text = UserTextCreateMutation.Field()
    update_user_text = UserTextUpdateMutation.Field()
    delete_user_text = UserTextDeleteMutation.Field()
    create_comment = CommentCreateMutation.Field()
    update_comment = CommentUpdateMutation.Field()
    delete_comment = CommentDeleteMutation.Field()