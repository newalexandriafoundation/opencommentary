import httpretty

from django.conf import settings
from django.test import RequestFactory, TestCase

from alexandria_app.models import Project, CustomUser
from ..lib.render_react_ssr_view import set_ssr_content, set_user_permissions


class ReactSsrViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = CustomUser.objects.create(
            username="Test", email="test@test.com", password="password")
        self.project = Project.objects.create(
            about_html="", hostname="test", title="title")
        self.project.admins.set([self.user])

    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_render(self):
        httpretty.register_uri(
            httpretty.POST,
            settings.RENDERING_SERVER_URL + "/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30",
            body='{"html": "<!doctype html><html><head></head><body></body></html>", "css": ""}')

        request = self.factory.get(
            "/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30",
            HTTP_HOST="test.beta.newalexandria.info"
        )
        request.user = self.user
        context = {
            "urn": "urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30"
        }

        set_user_permissions(request, context=context)

        self.assertEqual(context['USER_IS_ADMIN'], 1)

        set_ssr_content(request, context=context)

        self.assertEqual(context['USER_IS_ADMIN'], 1)
        self.assertEqual(context['REACT_APP'], "<!doctype html><html><head></head><body></body></html>")
