import json
from django.test import RequestFactory, TestCase

from alexandria_app.models import Bookmark, Project, CustomUser
from ..views import bookmarks, bookmarks_delete


def create_bookmark(project, user):
    return Bookmark.objects.create(
        urn="urn:alexandria:test:bookmarks", project=project, user=user)


class BookmarkTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = CustomUser.objects.create(
            username="Test", email="test@test.com", password="password")
        self.project = Project.objects.create(
            about_html="", hostname="", title="title")
        self.project.admins.set([self.user])

    def test_create(self):
        request = self.factory.post(
            '/bookmarks', {
                'urn': 'urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30'},
            HTTP_HOST='alexandria.local',
            content_type='application/json')
        request.user = self.user
        response = bookmarks(request)

        self.assertEqual(response.status_code, 201)

    def test_create_with_title(self):
        request = self.factory.post(
            '/bookmarks', {
                'urn': 'urn:cts:greekLit:tlg0012.tlg001.perseus-grc2:1.1-1.30',
                'title': 'Iliad'},
            HTTP_HOST='alexandria.local',
            content_type='application/json')
        request.user = self.user
        response = bookmarks(request)

        self.assertEqual(response.status_code, 201)

    def test_read(self):
        bookmark = create_bookmark(self.project, self.user)
        request = self.factory.get('/bookmarks', HTTP_HOST='alexandria.local')
        request.user = self.user
        response = bookmarks(request)

        self.assertEqual(response.status_code, 200)
        bms = json.loads(response.content)['bookmarks']
        self.assertEqual(bms[0]['pk'], bookmark.id)

    def test_delete(self):
        bookmark = create_bookmark(self.project, self.user)
        request = self.factory.delete(
            f'/bookmarks/{bookmark.id}', HTTP_HOST='alexandria.local')
        request.user = self.user
        response = bookmarks_delete(request, bookmark.id)

        self.assertEqual(response.status_code, 200)
        with self.assertRaises(Bookmark.DoesNotExist):
            Bookmark.objects.get(id=bookmark.id)
